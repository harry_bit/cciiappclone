package com.ccii.model;

/**
 * Created by shahr_000 on 28-08-2016.
 */
public class RequestSurveyEntryData {

    public  String user_id;
    public  String id;
    public  String house_holder_name;
    public  String adhaar_no;
    public  String munciple_holding_no;
    public  String voter_list_serial_no;
    public  String house_no;
    public  String employee_type;
    public  String payment;
    public  String ration_card;
    public  String water_arrangement;
    public  String house_pakka_kacha;
    public  String toilet_pakka_kacha;
    public  String elctricity;
    public  String cast_type;
    public  String criteria;
    public  String religion;
    public  String panchayat_munciple_tax;
    public  String house_type;
    public  String govt_loan;
    public  String voter_identity_card;
    public  String govt_job_card;
    public  String agricultural_land;
    public  String area_of_living_sqrft;
    public  String members;
    public  String adults;
    public  String minors;
    public  String court_case;
    public  String domestic_animal;
    public  String medical_facility;
    public  String tentanted;
    public  String member_of_tentanted;
    public  String lat;
    public  String lng;
    public  String mobile_no;
    public  String pin_code;
    public  String full_address;
    public  String profile_image;
    public  String house_image1;
    public  String house_image2;
    public  String house_image3;
}
