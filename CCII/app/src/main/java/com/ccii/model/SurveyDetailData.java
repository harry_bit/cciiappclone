package com.ccii.model;

/**
 * Created by Ravi on 25-07-2016.
 */
public class SurveyDetailData {

    public String house_holder_name;
    public String adhaar_no;
    public String munciple_holding_no;
    public String voter_list_serial_no;
    public String house_no;
    public String employee_type;
    public String ration_card;
    public String water_arrangment;
    public String house_pakka_kacha;
    public String toilet_pakka_kacha;
    public String elctricity;
    public String cast_type;
    public String criteria;
    public String religion;
    public String panchayat_munciple_tax;
    public String govt_loan;
    public String voter_identity_card;
    public String govt_job_card;
    public String agricultural_land;
    public String area_of_living_sqrft;
    public String members;
    public String adults;
    public String minors;
    public String court_case;
    public String domestic_animal;
    public String medical_facility;
    public String tentanted;
    public String mobile_no;
    public String pin_code;
    public String full_address;
    public String member_of_tentanted;
    public String house_image1;
    public String house_image2;
    public String house_image3;
    public String lat;
    public String lng;
    public String date;
    public String user_image;
    public String house_type;
    public String payment;
}
