package com.ccii.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ankita on 6/2/16.
 */
public class GeoCodeJSONParser {
    private String TAG = GeoCodeJSONParser.class.getSimpleName();
    private String status;

    /**
     * Receives a JSONObject and returns a list
     */
    public List<HashMap<String, String>> parse(JSONObject jObject) {

        JSONArray jPlaces = null;
        try {
            /** Retrieves all the elements in the 'places' array */
            jPlaces = jObject.getJSONArray("results");
            status = jObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /**
         * Invoking getPlaces with the array of json object where each json
         * object represent a place
         */
        return getPlaces(jPlaces);
    }

    private List<HashMap<String, String>> getPlaces(JSONArray jPlaces) {
        int placesCount = jPlaces.length();
        List<HashMap<String, String>> placesList = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> place;

        /** Taking each place, parses and adds to list object */
        for (int i = 0; i < placesCount; i++) {
            try {
                /** Call getPlace with place JSON object to parse the place */
                place = getPlace((JSONObject) jPlaces.get(i));
                placesList.add(place);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return placesList;
    }

    /**
     * Parsing the Place JSON object
     */
    private HashMap<String, String> getPlace(JSONObject jPlace) {

        HashMap<String, String> place = new HashMap<String, String>();

        String formatted_address;
        String placeId;
        String longName = "";
        String shortName = "";
        JSONArray types = null;

        try {
            formatted_address = jPlace.getString("formatted_address");
            placeId = jPlace.getString("place_id");

            JSONArray jsonArray = jPlace.getJSONArray("address_components");

            for (int i=0; i<jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                longName = jsonObject.getString("long_name");
                shortName = jsonObject.getString("short_name");
                types = jsonObject.getJSONArray("types");
            }

            place.put("formatted_address", formatted_address);
            place.put("place_id", placeId);

            for (int i=0; i< types.length(); i++){
                Log.e(TAG, "" +  types.getString(i));
                if(types.getString(i).equals("postal_code")){
                    place.put("long_name", longName);
                    place.put("short_name", shortName);
                    place.put("status", status);

                    /*Log.e(TAG, "longName:" + longName);
                    Log.e(TAG, "shortName:" + shortName);*/
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return place;
    }

}