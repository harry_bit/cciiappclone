package com.ccii.location;

import java.io.Serializable;
import java.util.List;


public class Legs implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<Steps> steps;
	public Distance distance;
	public DurationData duration;
	public EndLocation end_location;
	public StartLocation start_location;

}
