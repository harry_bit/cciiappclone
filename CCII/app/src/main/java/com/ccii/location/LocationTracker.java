package com.ccii.location;


import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;

import com.ccii.utils.LocationResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
/*
* This class is used for Getting location form GPS */


public class LocationTracker implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	 private LocationRequest mLocationRequest;
	 private GoogleApiClient mLocationClient;
	 SharedPreferences mPrefs;
     SharedPreferences.Editor mEditor;
     private Context context;
	 boolean mUpdatesRequested = false;
	 private LocationResult locationResult;

	 public LocationTracker(Context context, LocationResult locationResult) {
		super();
		this.context = context;
	    this.locationResult=locationResult;
	   
	 }

	
	  public void onUpdateLocation(){
			 
	         mLocationRequest = new LocationRequest();
	         mLocationRequest.setInterval(1000);
	         mLocationRequest.setFastestInterval(1000);
	         mLocationRequest.setSmallestDisplacement(100);
	         mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	         mLocationClient = new GoogleApiClient.Builder(context,LocationTracker.this, LocationTracker.this)
	        .addConnectionCallbacks(this)
	        .addOnConnectionFailedListener(this)
	        .addApi(LocationServices.API)
	        .build();
	         
	         
	         if(mLocationClient.isConnected()){
	        		startUpdates();
	         }else{
	             mLocationClient.connect();
	         }
	
	  }
	 
	 

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {

		startUpdates();

	}

	

	@Override
	public void onLocationChanged(Location location) {
	
		if(location!=null){
		
		    	if(location.getLatitude()!=0 && location.getLongitude()!=0){

							locationResult.gotLocation(location);

		     }
		   } 
	
	}
/**
     * In response to a request to start updates, send a request
     * to Location Services
     */

    private void startPeriodicUpdates() {
    	
    	 LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, LocationTracker.this);
       
    }

/**
     * In response to a request to stop updates, send a request to
     * Location Services*/


    private void stopPeriodicUpdates() {
    	  	LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, LocationTracker.this);
       
    }
    
    
    

    public void startUpdates() {
        mUpdatesRequested = true;
        startPeriodicUpdates();
        
    }
    
    
    
    
    


	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}


	public void stopUpdate() {
		if(mLocationClient!=null && mLocationClient.isConnected()) {
			stopPeriodicUpdates();
		}
	}
}
