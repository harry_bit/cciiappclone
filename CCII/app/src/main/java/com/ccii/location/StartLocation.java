package com.ccii.location;

import java.io.Serializable;

public class StartLocation implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double lat;
	public double lng;
}
