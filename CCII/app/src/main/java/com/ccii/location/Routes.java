package com.ccii.location;

import java.io.Serializable;
import java.util.List;

public class Routes implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<Legs> legs;
	public OverviewPolyline overview_polyline;
	public String summary;
}
