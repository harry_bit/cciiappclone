package com.ccii.location;

import java.io.Serializable;

public class Steps implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public PolylineData polyline;
	public Distance distance;
	public DurationData duration;
	public String html_instructions;
	public EndLocation end_location;
	public StartLocation start_location;
}
