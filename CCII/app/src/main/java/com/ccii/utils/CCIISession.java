package com.ccii.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ravi.shah.
 */
public class CCIISession {

    private static final String SHARED = "ccii_Preferences";
    private static final String USER_ID = "userID";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String IS_LOGIN = "isLogin";
    public static final String DEVICE_TOKEN = "deviceTokne";


    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private Context context;

    public CCIISession(Context context) {
        this.context = context;
        sharedPref = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void setSession(String userId, String accessToken, boolean isLogin) {
        editor.putString(USER_ID, userId);
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }


    public String getUserId() {
        return sharedPref.getString(USER_ID, "");
    }

    public String getAccessToken() {
        return sharedPref.getString(ACCESS_TOKEN, "");
    }

    public boolean getIsLogin() {
        return sharedPref.getBoolean(IS_LOGIN, false);
    }

    public void setDeviceToken(String resgiterId){
        editor.putString(DEVICE_TOKEN, resgiterId);
        editor.commit();
    }

    public String getDeviceToken(){
        return sharedPref.getString(DEVICE_TOKEN, "");
    }


}