package com.ccii.utils;

public class Constants {


    public static final String PREF_USER_INFO = "userinfo";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_DELETE = "DELETE";

    public static final String IS_LOGIN = "is_login";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";
    public static final String RED_FONT = "<font color='#FF0000'>*</font>";

    public static final int PERMISSION_REQUEST_CODE = 100;//for permissions

    public static final String SENDER_ID = "253131473950";

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static String SURVEY_ID_VISIBLE = "SURVEY_ID_VISIBLE";
    public interface PREF_KEYS {
        final String LOGIN_STATUS = "LOGIN_STATUS";



    }



}