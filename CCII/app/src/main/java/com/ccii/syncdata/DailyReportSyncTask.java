package com.ccii.syncdata;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ccii.R;
import com.ccii.activity.HomeActivity;
import com.ccii.database.SurveyEntryTable;
import com.ccii.model.RequestSurveyEntryData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.servicehelper.WebApiConnection;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravi.shah on 3/9/16.
 */

public class DailyReportSyncTask extends AsyncTask<Object, Void, Object> {

    private Context context;
    private ProgressDialog progressDialog;
    private List<RequestSurveyEntryData> arlSurveyEntryList;
    private LinearLayout llMainView;
    private int syncCount = 0;
    private AlertDialog alert;

    public DailyReportSyncTask(Context context, List<RequestSurveyEntryData> arlSurveyEntryList, LinearLayout llMainView) {
        this.context = context;
        this.arlSurveyEntryList = arlSurveyEntryList;
        this.llMainView = llMainView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, null, "Please wait, syncing data...");
    }

    @Override
    protected Object doInBackground(Object... objects) {
        if(arlSurveyEntryList!=null && arlSurveyEntryList.size()>0){
            for (int i = 0; i < arlSurveyEntryList.size(); i++) {
                surveryEntryAPI(arlSurveyEntryList.get(i));
            }
        }else{
           // Toast.makeText(context, "No Data found.", Toast.LENGTH_LONG).show();
        }

        return null;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
        showAlert("Successfully uploaded");

    }

    private void surveryEntryAPI(RequestSurveyEntryData requestSurveyEntryData) {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("user_id", requestSurveyEntryData.user_id);
            jsonObject.addProperty("house_holder_name", requestSurveyEntryData.house_holder_name);
            jsonObject.addProperty("adhaar_no", requestSurveyEntryData.adhaar_no);
            jsonObject.addProperty("munciple_holding_no", requestSurveyEntryData.munciple_holding_no);
            jsonObject.addProperty("voter_list_serial_no", requestSurveyEntryData.voter_identity_card);
            jsonObject.addProperty("house_no", requestSurveyEntryData.house_no);
            jsonObject.addProperty("employee_type", requestSurveyEntryData.employee_type);
            jsonObject.addProperty("payment", requestSurveyEntryData.payment);
            jsonObject.addProperty("ration_card", requestSurveyEntryData.ration_card);
            jsonObject.addProperty("water_arrangement", requestSurveyEntryData.water_arrangement);
            jsonObject.addProperty("house_pakka_kacha", requestSurveyEntryData.house_pakka_kacha);
            jsonObject.addProperty("toilet_pakka_kacha", requestSurveyEntryData.toilet_pakka_kacha);
            jsonObject.addProperty("elctricity", requestSurveyEntryData.elctricity);
            jsonObject.addProperty("cast_type", requestSurveyEntryData.cast_type);
            jsonObject.addProperty("criteria", requestSurveyEntryData.criteria);
            jsonObject.addProperty("religion", requestSurveyEntryData.religion);
            jsonObject.addProperty("panchayat_munciple_tax", requestSurveyEntryData.panchayat_munciple_tax);
            jsonObject.addProperty("house_type", requestSurveyEntryData.house_type);
            jsonObject.addProperty("panchayat_munciple_tax", requestSurveyEntryData.panchayat_munciple_tax);
            jsonObject.addProperty("govt_loan", requestSurveyEntryData.govt_loan);
            jsonObject.addProperty("voter_identity_card", requestSurveyEntryData.voter_identity_card);
            jsonObject.addProperty("govt_job_card", requestSurveyEntryData.govt_job_card);
            jsonObject.addProperty("agricultural_land", requestSurveyEntryData.agricultural_land);
            jsonObject.addProperty("area_of_living_sqrft", requestSurveyEntryData.area_of_living_sqrft);
            jsonObject.addProperty("members", requestSurveyEntryData.members);
            jsonObject.addProperty("adults", requestSurveyEntryData.adults);
            jsonObject.addProperty("minors", requestSurveyEntryData.minors);
            jsonObject.addProperty("court_case", requestSurveyEntryData.court_case);
            jsonObject.addProperty("domestic_animal", requestSurveyEntryData.domestic_animal);
            jsonObject.addProperty("medical_facility", requestSurveyEntryData.medical_facility);
            jsonObject.addProperty("tentanted", requestSurveyEntryData.tentanted);
            jsonObject.addProperty("member_of_tentanted", requestSurveyEntryData.member_of_tentanted);
            jsonObject.addProperty("lat", requestSurveyEntryData.lat);
            jsonObject.addProperty("lng", requestSurveyEntryData.lng);
            jsonObject.addProperty("mobile_no", requestSurveyEntryData.mobile_no);
            jsonObject.addProperty("pin_code", requestSurveyEntryData.pin_code);
            jsonObject.addProperty("full_address", requestSurveyEntryData.full_address);


            String url = RequestURL.SURVEY_ENTRY;

            CommonUtils.printLog("", "________________Request url___________" + url);


            WebApiConnection serviceCall = new WebApiConnection(context);
            String response = serviceCall.getServiceResponse(url, jsonObject.toString(), Constants.METHOD_POST);


        try {
            if (response != null) {
                CommonUtils.printLog("", "________________Response___________ " + response);
                JsonResponse jsonResponse = new Gson().fromJson(response, JsonResponse.class);
                if(jsonResponse!=null){
                    if(jsonResponse.status.equalsIgnoreCase("true")){

                        //For User Image
                        String base64UserImage = "";
                        Bitmap bitmapuserImage = null;
                        File userFile = new File(requestSurveyEntryData.profile_image);
                        try {
                            if(userFile!=null && userFile.exists()) {
                                bitmapuserImage = decodeFile(userFile);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(!CommonUtils.CheckIfNull(bitmapuserImage)){
                            base64UserImage = encodeTobase64(bitmapuserImage);
                            Log.e("","base64UserImage "+ base64UserImage);
                        }else{
                            base64UserImage = "";
                        }

                        if(jsonResponse.survey_id!=null && jsonResponse.survey_id.length()>0){
                            uploadImageAPI(jsonResponse.survey_id, "user_image", base64UserImage, requestSurveyEntryData);
                        }

                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    private void uploadImageAPI(String surveyId, String imageType, String base64, RequestSurveyEntryData requestSurveyEntryData) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("survey_id", surveyId);
        jsonObject.addProperty("image", imageType);
        jsonObject.addProperty("data", base64);

        String url = RequestURL.PHOTO_UPLOAD;

        WebApiConnection serviceCall = new WebApiConnection(context);
        String response = serviceCall.getServiceResponse(url, jsonObject.toString(), Constants.METHOD_POST);


        try {
            if (response != null) {
                CommonUtils.printLog("", "________________Response___________ " + response);
                JsonResponse jsonResponse = new Gson().fromJson(response, JsonResponse.class);
                if (jsonResponse != null) {
                    if (jsonResponse.status.equalsIgnoreCase("true")) {
                        if(requestSurveyEntryData.house_image1!=null && requestSurveyEntryData.house_image1.length()>0){
                            //For Home1 Image
                            String base64Home1Image = "";
                            Bitmap bitmapHome1Image = null;
                            File home1File = new File(requestSurveyEntryData.house_image1);
                            try {
                                if(home1File!=null && home1File.exists()) {
                                    bitmapHome1Image = decodeFile(home1File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome1Image)){
                                base64Home1Image = encodeTobase64(bitmapHome1Image);
                                Log.e("","base64Home1Image "+ base64Home1Image);
                            }else{
                                base64Home1Image = "";
                            }

                            uploadHouseImage1API(surveyId, "house_image1", base64Home1Image, requestSurveyEntryData);

                        }else if(requestSurveyEntryData.house_image2!=null && requestSurveyEntryData.house_image2.length()>0){
                            String base64Home2Image = "";
                            Bitmap bitmapHome2Image = null;
                            File home2File = new File(requestSurveyEntryData.house_image2);
                            try {
                                if(home2File!=null && home2File.exists()) {
                                    bitmapHome2Image = decodeFile(home2File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome2Image)){
                                base64Home2Image = encodeTobase64(bitmapHome2Image);
                                Log.e("","base64Home2Image "+ base64Home2Image);
                            }else{
                                base64Home2Image = "";
                            }

                            uploadHouseImage2API(surveyId, "house_image2", base64Home2Image,requestSurveyEntryData);

                        }else  if(requestSurveyEntryData.house_image3!=null && requestSurveyEntryData.house_image3.length()>0){
                            //For Home3 Image
                            String base64Home3Image = "";
                            Bitmap bitmapHome3Image = null;
                            File home3File = new File(requestSurveyEntryData.house_image3);
                            try {
                                if(home3File!=null && home3File.exists()) {
                                    bitmapHome3Image = decodeFile(home3File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                base64Home3Image = encodeTobase64(bitmapHome3Image);
                                Log.e("","base64Home3Image "+ base64Home3Image);
                            }else{
                                base64Home3Image = "";
                            }

                            uploadHouseImage3API(surveyId, "house_image3", base64Home3Image, requestSurveyEntryData);
                        }else{
                            try {
                                SurveyEntryTable surveyEntryTable = new SurveyEntryTable(context);
                                surveyEntryTable.deleteRow(requestSurveyEntryData.id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            syncCount +=1;
                            Handler h = new Handler(Looper.getMainLooper());
                            h.post(new Runnable() {
                                public void run() {
                                    Toast.makeText(context,"Uploaded "+ syncCount+"/"+arlSurveyEntryList.size(), Toast.LENGTH_SHORT).show();
                                }
                            });


                        }

                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    private void uploadHouseImage3API(String surveyId, String imageType, String base64, RequestSurveyEntryData requestSurveyEntryData) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("survey_id", surveyId);
        jsonObject.addProperty("image", imageType);
        jsonObject.addProperty("data", base64);

        String url = RequestURL.PHOTO_UPLOAD;

        WebApiConnection serviceCall = new WebApiConnection(context);
        String response = serviceCall.getServiceResponse(url, jsonObject.toString(), Constants.METHOD_POST);

        try {
            if (response != null) {
                CommonUtils.printLog("", "________________Response___________ " + response);
                JsonResponse jsonResponse = new Gson().fromJson(response, JsonResponse.class);
                if (jsonResponse != null) {
                    if (jsonResponse.status.equalsIgnoreCase("true")) {
                        try {
                            SurveyEntryTable surveyEntryTable = new SurveyEntryTable(context);
                            surveyEntryTable.deleteRow(requestSurveyEntryData.id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        syncCount +=1;
                        Handler h = new Handler(Looper.getMainLooper());
                        h.post(new Runnable() {
                            public void run() {
                                Toast.makeText(context,"Uploaded "+ syncCount+"/"+arlSurveyEntryList.size(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }


    private void uploadHouseImage2API(String surveyId, String imageType, String base64, RequestSurveyEntryData requestSurveyEntryData) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("survey_id", surveyId);
        jsonObject.addProperty("image", imageType);
        jsonObject.addProperty("data", base64);

        String url = RequestURL.PHOTO_UPLOAD;

        WebApiConnection serviceCall = new WebApiConnection(context);
        String response = serviceCall.getServiceResponse(url, jsonObject.toString(), Constants.METHOD_POST);

        try {
            if (response != null) {
                CommonUtils.printLog("", "________________Response___________ " + response);
                JsonResponse jsonResponse = new Gson().fromJson(response, JsonResponse.class);
                if (jsonResponse != null) {
                    if (jsonResponse.status.equalsIgnoreCase("true")) {


                        if(requestSurveyEntryData.house_image3!=null && requestSurveyEntryData.house_image3.length()>0){
                            //For Home3 Image
                            String base64Home3Image = "";
                            Bitmap bitmapHome3Image = null;
                            File home3File = new File(requestSurveyEntryData.house_image3);
                            try {
                                if(home3File!=null && home3File.exists()) {
                                    bitmapHome3Image = decodeFile(home3File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                base64Home3Image = encodeTobase64(bitmapHome3Image);
                                Log.e("","base64Home3Image "+ base64Home3Image);
                            }else{
                                base64Home3Image = "";
                            }
                            uploadHouseImage3API(surveyId, "house_image3", base64Home3Image, requestSurveyEntryData);
                        }else{
                            try {
                                SurveyEntryTable surveyEntryTable = new SurveyEntryTable(context);
                                surveyEntryTable.deleteRow(requestSurveyEntryData.id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            syncCount +=1;
                            Handler h = new Handler(Looper.getMainLooper());
                            h.post(new Runnable() {
                                public void run() {
                                    Toast.makeText(context,"Uploaded "+ syncCount+"/"+arlSurveyEntryList.size(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }



    }

    private void uploadHouseImage1API(String surveyId, String imageType, String base64, RequestSurveyEntryData requestSurveyEntryData) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("survey_id", surveyId);
        jsonObject.addProperty("image", imageType);
        jsonObject.addProperty("data", base64);

        String url = RequestURL.PHOTO_UPLOAD;

        WebApiConnection serviceCall = new WebApiConnection(context);
        String response = serviceCall.getServiceResponse(url, jsonObject.toString(), Constants.METHOD_POST);

        try {
            if (response != null) {
                CommonUtils.printLog("", "________________Response___________ " + response);
                JsonResponse jsonResponse = new Gson().fromJson(response, JsonResponse.class);
                if (jsonResponse != null) {
                    if (jsonResponse.status.equalsIgnoreCase("true")) {

                        if(requestSurveyEntryData.house_image2!=null && requestSurveyEntryData.house_image2.length()>0){
//For Home2 Image
                            String base64Home2Image = "";
                            Bitmap bitmapHome2Image = null;
                            File home2File = new File(requestSurveyEntryData.house_image2);
                            try {
                                if(home2File!=null && home2File.exists()) {
                                    bitmapHome2Image = decodeFile(home2File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome2Image)){
                                base64Home2Image = encodeTobase64(bitmapHome2Image);
                                Log.e("","base64Home2Image "+ base64Home2Image);
                            }else{
                                base64Home2Image = "";
                            }

                            uploadHouseImage2API(surveyId, "house_image2", base64Home2Image, requestSurveyEntryData);
                        }else  if(requestSurveyEntryData.house_image3!=null && requestSurveyEntryData.house_image3.length()>0){
                            //For Home3 Image
                            String base64Home3Image = "";
                            Bitmap bitmapHome3Image = null;
                            File home3File = new File(requestSurveyEntryData.house_image3);
                            try {
                                if(home3File!=null && home3File.exists()) {
                                    bitmapHome3Image = decodeFile(home3File);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                base64Home3Image = encodeTobase64(bitmapHome3Image);
                                Log.e("","base64Home3Image "+ base64Home3Image);
                            }else{
                                base64Home3Image = "";
                            }
                            uploadHouseImage3API(surveyId, "house_image3", base64Home3Image, requestSurveyEntryData);
                        }else{
                            try {
                                SurveyEntryTable surveyEntryTable = new SurveyEntryTable(context);
                                surveyEntryTable.deleteRow(requestSurveyEntryData.id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            syncCount +=1;
                            Handler h = new Handler(Looper.getMainLooper());
                            h.post(new Runnable() {
                                public void run() {
                                    Toast.makeText(context,"Uploaded "+ syncCount+"/"+arlSurveyEntryList.size(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }


    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=720;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    public static String encodeTobase64(Bitmap image)
    {
        String encodedImage = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArrayImage = baos.toByteArray();
            encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
            Log.d("base64Encode", encodedImage);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedImage;
    }

    private void showAlert(String msg){
        if(alert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            alert = builder.create();
            alert.show();
        }else{
            try {
                if(!alert.isShowing()){
                    alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
