package com.ccii.database;

/**
 * Created by shahr_000 on 28-08-2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Base64;

import com.ccii.model.RequestSurveyEntryData;
import com.ccii.responsedata.UserData;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ravi.shah on 15/7/16.
 */
public class SurveyEntryTable {
    private final static String CREATE_TABLE_SURVEY_ENTRY = "CREATE  TABLE  IF NOT EXISTS " + DBConstant.TBL_SURVEY_ENTRY +
            " ( " + DBConstant.USER_ID + " VARCHAR NOT NULL ," + DBConstant.KEY_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ," +
            DBConstant.ADHAR_NO + " VARCHAR ," + DBConstant.HOUSE_HOLDER_NAME + " VARCHAR ," + DBConstant.MUNCIPLE_HOLDING_NO + " VARCHAR ," +
            DBConstant.VOTER_LIST_SERIAL_NO + " VARCHAR ," + DBConstant.HOUSE_NO + " VARCHAR ," + DBConstant.EMPLOYEE_TYPE + " VARCHAR ," +
            DBConstant.PAYMENT + " VARCHAR ," + DBConstant.RATION_CARD + " VARCHAR ," +
            DBConstant.WATER_ARRANGMEMENT + " VARCHAR ," + DBConstant.HOUSE_PAKKA_KACHA + " VARCHAR ," + DBConstant.TOILET_PAKKA_KACHA + " VARCHAR ," +
            DBConstant.ELCTRICITY + " VARCHAR ," + DBConstant.CAST_TYPE + " VARCHAR ," + DBConstant.CRITERIA + " VARCHAR ," +
            DBConstant.RELIGION + " VARCHAR ," + DBConstant.PANCHAYAT_MUNCIPLE_TAX + " VARCHAR ," + DBConstant.HOUSE_TYPE + " VARCHAR ," +
            DBConstant.GOVT_LOAN + " VARCHAR ," + DBConstant.VOTER_IDENTITY + " VARCHAR ," + DBConstant.GOVT_JOB_CARD + " VARCHAR ," +
            DBConstant.AGRICULTURAL_LAND + " VARCHAR ," + DBConstant.AREA_OF_LIVING_SQRFT + " VARCHAR ," + DBConstant.MEMBER + " VARCHAR ," +
            DBConstant.ADULTS + " VARCHAR ," + DBConstant.MINORS + " VARCHAR ," + DBConstant.COURT_CASE + " VARCHAR ," +
            DBConstant.DOMESTIV_ANIMAL + " VARCHAR ," + DBConstant.MEDICAL_FACILITY + " VARCHAR ," + DBConstant.TENTANTED + " VARCHAR ," +
            DBConstant.MEMBER_OF_TENTANTED + " VARCHAR ," + DBConstant.LAT + " VARCHAR ," + DBConstant.LNG + " VARCHAR ," +
            DBConstant.MOBILE_NO + " VARCHAR ," + DBConstant.PIN_CODE + " VARCHAR ," + DBConstant.FULL_ADDRESS + " VARCHAR ," +
            DBConstant.PROFILE_IMAGE + " VARCHAR ," + DBConstant.HOUSE_IMAGE1 + " VARCHAR ," + DBConstant.HOUSE_IMAGE2 + " VARCHAR ," + DBConstant.HOUSE_IMAGE3 + " VARCHAR ," +
            DBConstant.WARD + " VARCHAR)";

    private SQLiteDatabase myDataBase;

    public SurveyEntryTable(Context context) {
        new DbHelper(context);
        try {
            myDataBase = context.openOrCreateDatabase(DBConstant.DATABASE_NAME, DBConstant.DATABASE_VERSION, null);
            myDataBase.execSQL(CREATE_TABLE_SURVEY_ENTRY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_SURVEY_ENTRY);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        database.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_SURVEY_ENTRY);
        onCreate(database);
    }

    public void deleteRow(String id) {

        myDataBase.delete(DBConstant.TBL_SURVEY_ENTRY, "rowid =? ", new String[]{id});

    }

    public void insertToTable(RequestSurveyEntryData requestSurveyEntryData, String userId) {

        myDataBase.execSQL(CREATE_TABLE_SURVEY_ENTRY);

        ContentValues contentValue = new ContentValues();
        contentValue.put(DBConstant.USER_ID, userId);
        contentValue.put(DBConstant.HOUSE_HOLDER_NAME, requestSurveyEntryData.house_holder_name);
        contentValue.put(DBConstant.ADHAR_NO, requestSurveyEntryData.adhaar_no);
        contentValue.put(DBConstant.MUNCIPLE_HOLDING_NO, requestSurveyEntryData.munciple_holding_no);
        contentValue.put(DBConstant.VOTER_LIST_SERIAL_NO, requestSurveyEntryData.voter_list_serial_no);
        contentValue.put(DBConstant.HOUSE_NO, requestSurveyEntryData.house_no);
        contentValue.put(DBConstant.EMPLOYEE_TYPE, requestSurveyEntryData.employee_type);
        contentValue.put(DBConstant.PAYMENT, requestSurveyEntryData.payment);
        contentValue.put(DBConstant.RATION_CARD, requestSurveyEntryData.ration_card);
        contentValue.put(DBConstant.WATER_ARRANGMEMENT, requestSurveyEntryData.water_arrangement);
        contentValue.put(DBConstant.HOUSE_PAKKA_KACHA, requestSurveyEntryData.house_pakka_kacha);
        contentValue.put(DBConstant.TOILET_PAKKA_KACHA, requestSurveyEntryData.toilet_pakka_kacha);
        contentValue.put(DBConstant.ELCTRICITY, requestSurveyEntryData.elctricity);
        contentValue.put(DBConstant.CAST_TYPE, requestSurveyEntryData.cast_type);
        contentValue.put(DBConstant.CRITERIA, requestSurveyEntryData.criteria);
        contentValue.put(DBConstant.RELIGION, requestSurveyEntryData.religion);
        contentValue.put(DBConstant.PANCHAYAT_MUNCIPLE_TAX, requestSurveyEntryData.panchayat_munciple_tax);
        contentValue.put(DBConstant.HOUSE_TYPE, requestSurveyEntryData.house_type);
        contentValue.put(DBConstant.GOVT_LOAN, requestSurveyEntryData.govt_loan);
        contentValue.put(DBConstant.VOTER_IDENTITY, requestSurveyEntryData.voter_identity_card);
        contentValue.put(DBConstant.GOVT_JOB_CARD, requestSurveyEntryData.govt_job_card);
        contentValue.put(DBConstant.AGRICULTURAL_LAND, requestSurveyEntryData.agricultural_land);
        contentValue.put(DBConstant.AREA_OF_LIVING_SQRFT, requestSurveyEntryData.area_of_living_sqrft);
        contentValue.put(DBConstant.MEMBER, requestSurveyEntryData.members);
        contentValue.put(DBConstant.ADULTS, requestSurveyEntryData.adults);
        contentValue.put(DBConstant.MINORS, requestSurveyEntryData.minors);
        contentValue.put(DBConstant.COURT_CASE, requestSurveyEntryData.court_case);
        contentValue.put(DBConstant.DOMESTIV_ANIMAL, requestSurveyEntryData.domestic_animal);
        contentValue.put(DBConstant.MEDICAL_FACILITY, requestSurveyEntryData.medical_facility);
        contentValue.put(DBConstant.TENTANTED, requestSurveyEntryData.tentanted);
        contentValue.put(DBConstant.MEMBER_OF_TENTANTED, requestSurveyEntryData.member_of_tentanted);
        contentValue.put(DBConstant.LAT, requestSurveyEntryData.lat);
        contentValue.put(DBConstant.LNG, requestSurveyEntryData.lng);
        contentValue.put(DBConstant.MOBILE_NO, requestSurveyEntryData.mobile_no);
        contentValue.put(DBConstant.PIN_CODE, requestSurveyEntryData.pin_code);
        contentValue.put(DBConstant.FULL_ADDRESS, requestSurveyEntryData.full_address);
        contentValue.put(DBConstant.PROFILE_IMAGE, requestSurveyEntryData.profile_image);
        contentValue.put(DBConstant.HOUSE_IMAGE1, requestSurveyEntryData.house_image1);
        contentValue.put(DBConstant.HOUSE_IMAGE2, requestSurveyEntryData.house_image2);
        contentValue.put(DBConstant.HOUSE_IMAGE3, requestSurveyEntryData.house_image3);


        myDataBase.insertWithOnConflict(DBConstant.TBL_SURVEY_ENTRY, null, contentValue, SQLiteDatabase.CONFLICT_REPLACE);

    }



    public List<RequestSurveyEntryData> getAllSurveyEntryList(String userId) {
        List<RequestSurveyEntryData> arlSurveyEntryList = new ArrayList<RequestSurveyEntryData>();

        Cursor cursor = myDataBase.query(DBConstant.TBL_SURVEY_ENTRY, null, DBConstant.USER_ID + "=? ", new String[]{userId}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                RequestSurveyEntryData requestSurveyEntryData = new RequestSurveyEntryData();
                requestSurveyEntryData.id = String.valueOf(cursor.getInt(cursor.getColumnIndex(DBConstant.KEY_ID)));
                requestSurveyEntryData.user_id = cursor.getString(cursor.getColumnIndex(DBConstant.USER_ID));
                requestSurveyEntryData.house_holder_name = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_HOLDER_NAME));
                requestSurveyEntryData.adhaar_no = cursor.getString(cursor.getColumnIndex(DBConstant.ADHAR_NO));
                requestSurveyEntryData.munciple_holding_no = cursor.getString(cursor.getColumnIndex(DBConstant.MUNCIPLE_HOLDING_NO));
                requestSurveyEntryData.voter_list_serial_no = cursor.getString(cursor.getColumnIndex(DBConstant.VOTER_LIST_SERIAL_NO));
                requestSurveyEntryData.house_no = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_NO));
                requestSurveyEntryData.employee_type = cursor.getString(cursor.getColumnIndex(DBConstant.EMPLOYEE_TYPE));
                requestSurveyEntryData.payment = cursor.getString(cursor.getColumnIndex(DBConstant.PAYMENT));
                requestSurveyEntryData.ration_card = cursor.getString(cursor.getColumnIndex(DBConstant.RATION_CARD));
                requestSurveyEntryData.water_arrangement = cursor.getString(cursor.getColumnIndex(DBConstant.WATER_ARRANGMEMENT));
                requestSurveyEntryData.house_pakka_kacha = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_PAKKA_KACHA));
                requestSurveyEntryData.toilet_pakka_kacha = cursor.getString(cursor.getColumnIndex(DBConstant.TOILET_PAKKA_KACHA));
                requestSurveyEntryData.elctricity = cursor.getString(cursor.getColumnIndex(DBConstant.ELCTRICITY));
                requestSurveyEntryData.cast_type = cursor.getString(cursor.getColumnIndex(DBConstant.CAST_TYPE));
                requestSurveyEntryData.criteria = cursor.getString(cursor.getColumnIndex(DBConstant.CRITERIA));
                requestSurveyEntryData.religion = cursor.getString(cursor.getColumnIndex(DBConstant.RELIGION));
                requestSurveyEntryData.panchayat_munciple_tax = cursor.getString(cursor.getColumnIndex(DBConstant.PANCHAYAT_MUNCIPLE_TAX));
                requestSurveyEntryData.house_type = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_TYPE));
                requestSurveyEntryData.govt_loan = cursor.getString(cursor.getColumnIndex(DBConstant.GOVT_LOAN));
                requestSurveyEntryData.voter_identity_card = cursor.getString(cursor.getColumnIndex(DBConstant.VOTER_LIST_SERIAL_NO));
                requestSurveyEntryData.govt_job_card = cursor.getString(cursor.getColumnIndex(DBConstant.GOVT_JOB_CARD));
                requestSurveyEntryData.agricultural_land = cursor.getString(cursor.getColumnIndex(DBConstant.AGRICULTURAL_LAND));
                requestSurveyEntryData.area_of_living_sqrft = cursor.getString(cursor.getColumnIndex(DBConstant.AREA_OF_LIVING_SQRFT));
                requestSurveyEntryData.members = cursor.getString(cursor.getColumnIndex(DBConstant.MEMBER));
                requestSurveyEntryData.adults = cursor.getString(cursor.getColumnIndex(DBConstant.ADULTS));
                requestSurveyEntryData.minors = cursor.getString(cursor.getColumnIndex(DBConstant.MINORS));
                requestSurveyEntryData.court_case = cursor.getString(cursor.getColumnIndex(DBConstant.COURT_CASE));
                requestSurveyEntryData.domestic_animal = cursor.getString(cursor.getColumnIndex(DBConstant.DOMESTIV_ANIMAL));
                requestSurveyEntryData.medical_facility = cursor.getString(cursor.getColumnIndex(DBConstant.MEDICAL_FACILITY));
                requestSurveyEntryData.tentanted = cursor.getString(cursor.getColumnIndex(DBConstant.TENTANTED));
                requestSurveyEntryData.member_of_tentanted = cursor.getString(cursor.getColumnIndex(DBConstant.MEMBER_OF_TENTANTED));
                requestSurveyEntryData.lat = cursor.getString(cursor.getColumnIndex(DBConstant.LAT));
                requestSurveyEntryData.lng = cursor.getString(cursor.getColumnIndex(DBConstant.LNG));
                requestSurveyEntryData.mobile_no = cursor.getString(cursor.getColumnIndex(DBConstant.MOBILE_NO));
                requestSurveyEntryData.pin_code = cursor.getString(cursor.getColumnIndex(DBConstant.PIN_CODE));
                requestSurveyEntryData.full_address = cursor.getString(cursor.getColumnIndex(DBConstant.FULL_ADDRESS));
                requestSurveyEntryData.profile_image = cursor.getString(cursor.getColumnIndex(DBConstant.PROFILE_IMAGE));
                requestSurveyEntryData.house_image1 = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_IMAGE1));
                requestSurveyEntryData.house_image2 = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_IMAGE2));
                requestSurveyEntryData.house_image3 = cursor.getString(cursor.getColumnIndex(DBConstant.HOUSE_IMAGE3));

                arlSurveyEntryList.add(requestSurveyEntryData);

                cursor.moveToNext();
            }
            cursor.close();

        }
        return arlSurveyEntryList;
    }

}