package com.ccii.database;

/**
 * Created by shahr_000 on 28-08-2016.
 */

public class DBConstant {


    public static final String DATABASE_NAME = "ccii.db";
    public static final int DATABASE_VERSION = 1;


    //Tables name
    public static final String TBL_USER = "userTabel";
    public static final String TBL_SURVEY_ENTRY = "surveyEntryTable";


    //Profile Table
    public static final String KEY_ID = "keyId";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "username";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String USER_IMAGE = "thumbnail";
    public static final String PASSWORD = "password";//save base64 string
    public static final String DOB = "dob";
    public static final String STATE = "state";
    public static final String DISTRICT = "district";
    public static final String WARD = "ward";
    public static final String LOGIN_ID = "login_id";

    public static final String HOUSE_HOLDER_NAME = "house_holder_name";
    public static final String ADHAR_NO = "adhaar_no";
    public static final String MUNCIPLE_HOLDING_NO = "munciple_holding_no";
    public static final String VOTER_LIST_SERIAL_NO = "voter_list_serial_no";
    public static final String HOUSE_NO = "house_no";
    public static final String EMPLOYEE_TYPE = "employee_type";
    public static final String PAYMENT = "payment";
    public static final String RATION_CARD = "ration_card";
    public static final String WATER_ARRANGMEMENT = "water_arrangement";
    public static final String HOUSE_PAKKA_KACHA = "house_pakka_kacha";
    public static final String TOILET_PAKKA_KACHA = "toilet_pakka_kacha";
    public static final String ELCTRICITY = "elctricity";
    public static final String CAST_TYPE = "cast_type";
    public static final String CRITERIA = "criteria";
    public static final String RELIGION = "religion";
    public static final String PANCHAYAT_MUNCIPLE_TAX = "panchayat_munciple_tax";
    public static final String HOUSE_TYPE = "house_type";
    public static final String GOVT_LOAN = "govt_loan";
    public static final String VOTER_IDENTITY = "voter_identity_card";
    public static final String GOVT_JOB_CARD = "govt_job_card";
    public static final String AGRICULTURAL_LAND = "agricultural_land";
    public static final String AREA_OF_LIVING_SQRFT = "area_of_living_sqrft";
    public static final String MEMBER = "members";
    public static final String ADULTS = "adults";
    public static final String MINORS = "minors";
    public static final String COURT_CASE = "court_case";
    public static final String DOMESTIV_ANIMAL = "domestic_animal";
    public static final String MEDICAL_FACILITY = "medical_facility";
    public static final String TENTANTED = "tentanted";
    public static final String MEMBER_OF_TENTANTED = "member_of_tentanted";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String MOBILE_NO = "mobile_no";
    public static final String PIN_CODE = "pin_code";
    public static final String FULL_ADDRESS = "full_address";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String HOUSE_IMAGE1 = "house_image1";
    public static final String HOUSE_IMAGE2 = "house_image2";
    public static final String HOUSE_IMAGE3 = "house_image3";









}
