package com.ccii.database;

/**
 * Created by shahr_000 on 28-08-2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Base64;

import com.ccii.responsedata.UserData;


/**
 * Created by ravi.shah on 15/7/16.
 */
public class UserTable {
    private final static String CREATE_TABLE_USER = "CREATE  TABLE  IF NOT EXISTS " + DBConstant.TBL_USER +
            " ( " + DBConstant.USER_ID + " VARCHAR PRIMARY KEY  NOT NULL ," +
            DBConstant.USER_NAME + " VARCHAR ," + DBConstant.EMAIL + " VARCHAR ," + DBConstant.MOBILE + " VARCHAR ," +
            DBConstant.USER_IMAGE + " VARCHAR ," + DBConstant.PASSWORD + " VARCHAR ," + DBConstant.DOB + " VARCHAR ," +
            DBConstant.STATE + " VARCHAR ," + DBConstant.DISTRICT + " VARCHAR ," + DBConstant.LOGIN_ID + " VARCHAR ," +
            DBConstant.WARD + " VARCHAR)";

    private SQLiteDatabase myDataBase;

    public UserTable(Context context) {
        new DbHelper(context);
        try {
            myDataBase = context.openOrCreateDatabase(DBConstant.DATABASE_NAME, DBConstant.DATABASE_VERSION, null);
            myDataBase.execSQL(CREATE_TABLE_USER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_USER);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        database.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USER);
        onCreate(database);
    }

    public void insertToTable(UserData userDetailModel, String password, String login_id) {

        myDataBase.execSQL(CREATE_TABLE_USER);

        ContentValues contentValue = new ContentValues();
        contentValue.put(DBConstant.USER_ID, userDetailModel.userId);
        contentValue.put(DBConstant.USER_NAME, userDetailModel.username);
        contentValue.put(DBConstant.LOGIN_ID, login_id);
        contentValue.put(DBConstant.EMAIL, userDetailModel.email);
        contentValue.put(DBConstant.MOBILE, userDetailModel.mobile);
        contentValue.put(DBConstant.USER_IMAGE, userDetailModel.thumbnail);
        contentValue.put(DBConstant.PASSWORD, Base64.encodeToString(password.getBytes(), Base64.NO_WRAP));
        contentValue.put(DBConstant.DOB, userDetailModel.dob);
        contentValue.put(DBConstant.STATE, userDetailModel.state);
        contentValue.put(DBConstant.DISTRICT, userDetailModel.district);
        contentValue.put(DBConstant.WARD, userDetailModel.ward);

        myDataBase.insertWithOnConflict(DBConstant.TBL_USER, null, contentValue, SQLiteDatabase.CONFLICT_REPLACE);

    }

    public String thumbnail;
    public String userId;
    public String email;
    public String username;
    public String dob;
    public String mobile;
    public String state;
    public String district;
    public String ward;


    public UserData getUserDetail(String userId) {

        UserData userDetailModel = new UserData();

        Cursor mCursor = myDataBase.query(DBConstant.TBL_USER, null, DBConstant.USER_ID + "=? ", new String[]{userId}, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            while (!mCursor.isAfterLast()) {

                userDetailModel.userId = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_ID));
                userDetailModel.dob = mCursor.getString(mCursor.getColumnIndex(DBConstant.DOB));
                userDetailModel.username = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_NAME));
                userDetailModel.email = mCursor.getString(mCursor.getColumnIndex(DBConstant.EMAIL));
                userDetailModel.mobile = mCursor.getString(mCursor.getColumnIndex(DBConstant.MOBILE));
                userDetailModel.state = mCursor.getString(mCursor.getColumnIndex(DBConstant.STATE));
                userDetailModel.thumbnail = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_IMAGE));
                userDetailModel.district = mCursor.getString(mCursor.getColumnIndex(DBConstant.DISTRICT));
                userDetailModel.ward = mCursor.getString(mCursor.getColumnIndex(DBConstant.WARD));

                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return userDetailModel;
    }


    public UserData dbLoginUser(String loginId, String password) {
        String encodePassword = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);

        UserData userDetailModel = new UserData();
        Cursor mCursor = myDataBase.query(DBConstant.TBL_USER, null, DBConstant.LOGIN_ID + "=? AND " + DBConstant.PASSWORD + "=? ", new String[]{loginId, encodePassword}, null, null, null, null);

        if (mCursor != null && mCursor.moveToFirst()) {
            while (!mCursor.isAfterLast()) {
                userDetailModel.userId = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_ID));
                userDetailModel.dob = mCursor.getString(mCursor.getColumnIndex(DBConstant.DOB));
                userDetailModel.username = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_NAME));
                userDetailModel.email = mCursor.getString(mCursor.getColumnIndex(DBConstant.EMAIL));
                userDetailModel.mobile = mCursor.getString(mCursor.getColumnIndex(DBConstant.MOBILE));
                userDetailModel.state = mCursor.getString(mCursor.getColumnIndex(DBConstant.STATE));
                userDetailModel.thumbnail = mCursor.getString(mCursor.getColumnIndex(DBConstant.USER_IMAGE));
                userDetailModel.district = mCursor.getString(mCursor.getColumnIndex(DBConstant.DISTRICT));
                userDetailModel.ward = mCursor.getString(mCursor.getColumnIndex(DBConstant.WARD));

                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return userDetailModel;
    }

}