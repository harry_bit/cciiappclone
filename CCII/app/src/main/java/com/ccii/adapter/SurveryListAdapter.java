package com.ccii.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.activity.UpdateSurveyEntryActivity;
import com.ccii.model.SurveyListData;
import com.ccii.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by ravi.shah.
 */
public class SurveryListAdapter extends RecyclerView.Adapter<SurveryListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SurveyListData> arlSurveryList;

    public SurveryListAdapter(Context context, ArrayList<SurveyListData> arlSurveryList) {
        this.context = context;
        this.arlSurveryList = arlSurveryList;
    }

    @Override
    public SurveryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_survery_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SurveryListAdapter.ViewHolder holder, final int position) {


        /*if(arlAnnoucementsList.get(position).content!=null && arlAnnoucementsList.get(position).content.length()>0){
            holder.tvAnnouncementContents.setText(arlAnnoucementsList.get(position).content);
        }else{
            holder.tvAnnouncementContents.setText("");
        }*/

        if(arlSurveryList.get(position).house_holder_name!=null && arlSurveryList.get(position).house_holder_name.length()>0){
            holder.tvName.setText(arlSurveryList.get(position).house_holder_name);
        }else{
            holder.tvName.setText("");
        }

        if(arlSurveryList.get(position).date!=null && arlSurveryList.get(position).date.length()>0){

            holder.tvDate.setText(CommonUtils.getDateFormat(arlSurveryList.get(position).date));
        }else{
            holder.tvName.setText("");
        }

        holder.llMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateSurveyEntryActivity.class);
                intent.putExtra("surveyId", arlSurveryList.get(position).survey_id);
                intent.putExtra("mainSurveyId", arlSurveryList.get(position).main_survey_id);
                context.startActivity(intent);
            }
        });




    }

    @Override
    public int getItemCount() {
        return arlSurveryList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvDate;
        public LinearLayout llMainView;


        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            llMainView = (LinearLayout) itemView.findViewById(R.id.llMainView);

        }
    }

}