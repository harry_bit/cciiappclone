package com.ccii.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.responsedata.WardData;

import java.util.List;

/**
 * Created by ravi.shah
 */
public class WardTypeSpinnerAdapter extends BaseAdapter {

    private static final String TAG = WardTypeSpinnerAdapter.class.getSimpleName();
    private Context context;
    private List<String> arlDataList;
    private LayoutInflater infalter;


    public WardTypeSpinnerAdapter(Context context, List<String> arlDataList) {
        this.context = context;
        this.arlDataList = arlDataList;
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return arlDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return arlDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_item_spinner, null);
            holder = new ViewHolder();

            holder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            if(arlDataList.get(position)!=null && arlDataList.get(position).length()>0){
                holder.tvItemName.setText(arlDataList.get(position));

            }else{
                holder.tvItemName.setText("");
            }


        return convertView;
    }

    public class ViewHolder {
       TextView tvItemName;
    }




}