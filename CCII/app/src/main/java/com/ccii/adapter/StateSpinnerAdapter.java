package com.ccii.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.responsedata.StateData;

import java.util.List;

/**
 * Created by ravi.shah
 */
public class StateSpinnerAdapter extends BaseAdapter {

    private static final String TAG = StateSpinnerAdapter.class.getSimpleName();
    private Context context;
    private List<StateData> arlDataList;
    private LayoutInflater infalter;


    public StateSpinnerAdapter(Context context, List<StateData> arlDataList) {
        this.context = context;
        this.arlDataList = arlDataList;
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return arlDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return arlDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_item_spinner, null);
            holder = new ViewHolder();

            holder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            if(arlDataList.get(position).state!=null && arlDataList.get(position).state.length()>0){
                holder.tvItemName.setText(arlDataList.get(position).state);

            }else{
                holder.tvItemName.setText("");
            }


        return convertView;
    }

    public class ViewHolder {
       TextView tvItemName;
    }




}