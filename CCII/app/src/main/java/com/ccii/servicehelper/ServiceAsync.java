package com.ccii.servicehelper;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by ravi.shah on 30/3/16.
 */
public class ServiceAsync extends AsyncTask {
    private String request;
    private String url, methodName;
    private Context context;
    private ServiceStatus serviceStatus;

    public ServiceAsync(Context context, String request, String url, String methodName, ServiceStatus serviceStatus) {
        this.request = request;
        this.url = url;
        this.methodName = methodName;
        this.context = context;
        this.serviceStatus = serviceStatus;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        WebApiConnection serviceCall = new WebApiConnection(context);
        String response = serviceCall.getServiceResponse(url, request, methodName);
        Log.e("Response", " Response " + response);
        return response;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (o != null) {
            serviceStatus.onSuccess(o);
        } else {
            String message = "Could not connect to sever, Please try again.";
            serviceStatus.onFailed(message);
        }

    }
}
