package com.ccii.servicehelper;

/**
 * Created by ravi.shah on 30/3/16.
 */
public abstract class ServiceStatus {

    public abstract  void onSuccess(Object o);
    public abstract  void onFailed(Object o);

}
