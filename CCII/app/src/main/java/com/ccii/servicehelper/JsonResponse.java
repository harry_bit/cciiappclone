package com.ccii.servicehelper;


import com.ccii.model.SurveyDetailData;
import com.ccii.model.SurveyListData;
import com.ccii.responsedata.DistrictData;
import com.ccii.responsedata.StateData;
import com.ccii.responsedata.UserData;
import com.ccii.responsedata.WardData;

import java.util.List;

/**
 * Created by ravi.shah on 30/3/16.
 */
public class JsonResponse {

    public String status_code;
    public String status;
    public String message;
    public String userId;
    public String error;
    public String access_token;
    public String survey_id;
    public String code;

    public UserData user;

    public List<StateData> states;
    public List<DistrictData> districts;
    public List<WardData> wards;
    public List<SurveyListData> surveys;
    public SurveyDetailData survey;


}
