package com.ccii.servicehelper;


import android.content.Context;
import android.util.Log;

import com.ccii.utils.Constants;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This service is used for calling  APIs
 */

public class WebApiConnection {

    private int statusCode = 0;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    private String resMsg = null;
    private final static int CONNECTION_TIMEOUT = 20000;
    

    public WebApiConnection(Context mContext) {
    }


    public String getServiceResponse(String requestUrl, String request, String methodName) {
        //request = "requestData=" + request;
        Log.e("ServiceCall", "request " + request);
        Log.e("ServiceCall", "url " + requestUrl);
        HttpURLConnection conn = null;
        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            if (methodName.equalsIgnoreCase(Constants.METHOD_POST) || methodName.equalsIgnoreCase(Constants.METHOD_PUT)) {
                conn.setRequestMethod(methodName);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(request);
                writer.flush();
                writer.close();
                os.close();
            } else if (methodName.equalsIgnoreCase(Constants.METHOD_GET)) {
                conn.setRequestMethod(Constants.METHOD_GET);
            } else if (methodName.equalsIgnoreCase(Constants.METHOD_DELETE)) {
                conn.setRequestMethod(Constants.METHOD_DELETE);
            }
            int status = conn.getResponseCode();
            setStatusCode(status);
            InputStream in = null;
            Log.e("STATUS", "status code " + status);
            if (status >= HttpStatus.SC_BAD_REQUEST)
                in = new BufferedInputStream(conn.getErrorStream());
            else
                in = new BufferedInputStream(conn.getInputStream());
            String response = readStream(in);
            return response;
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace(); //If you want further info on failure...
            }
        }
        return null;
    }


    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }
}