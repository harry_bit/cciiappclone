package com.ccii.servicehelper;

/**
 * Created by ravi.shah on 30/3/16.
 */
public class RequestURL {

    //Local
    public static final String BASE_URL = "http://cciindiasurvey.in/api/";

    public static final String LOGIN = BASE_URL + "auth/post/login?";
    public static final String GET_STATE = BASE_URL + "state/get/state?country=IN";
    public static final String GET_DISTRICT = BASE_URL + "state/get/district?country=IN&state=";
    public static final String GET_WARD = BASE_URL + "state/get/ward?";
    public static final String LOGOUT = BASE_URL + "";
    public static final String SIGN_UP = BASE_URL + "auth/post/signup?";
    public static final String OTP = BASE_URL + "auth/post/otp";
    public static final String SURVEY_ENTRY = BASE_URL + "survey/post/survey";
    public static final String UPDATE_SURVEY_ENTRY = BASE_URL + "survey/put/survey";
    public static final String PHOTO_UPLOAD = BASE_URL + "survey/post/photo";
    public static final String SUREY_LIST = BASE_URL + "survey/get/list?";
    public static final String SUREY_DETAIL = BASE_URL + "survey/get/survey?";
    public static final String FORGOT_PASSWORD = BASE_URL + "auth/get/forgetPassword?mobile=";

    public static final int PERMISSION_REQUEST_CODE = 100;//for permissions




}
