package com.ccii.responsedata;

/**
 * Created by Ravi Shah on 7/18/2016.
 */
public class UserData {

    public String thumbnail;
    public String userId;
    public String email;
    public String username;
    public String dob;
    public String mobile;
    public String state;
    public String district;
    public String ward;
}
