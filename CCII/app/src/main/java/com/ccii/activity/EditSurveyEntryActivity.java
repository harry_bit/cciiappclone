/*
package com.ccii.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.adapter.SpinnerAdapter;
import com.ccii.cropimage.CropImage;
import com.ccii.model.SurveyDetailData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CircleTransformation;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.ccii.utils.LocationResult;
import com.ccii.utils.TakePictureUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

*/
/**
 * Created by Ravi Shah on 6/15/2016.
 *//*

public class EditSurveyEntryActivity extends Activity implements View.OnClickListener {

    private TextView tvSubmit, tvPrint;
    private EditText etHouseHolder,etAdharNumber, etMuncipalHoldingNo,etVoterListSerialNo, etMobileNumber, etPinCode, etHouseNumber, etFullAddress, etAreaofLivingHouse,etUnemployedBoys, etUnemployedGirls, etMemberofTenanted;
    private LinearLayout llMainView;

    private Spinner spinnerEmployeeType, spinnerCastType, spinnerCriteria, spinnerReligion;
    private SpinnerAdapter adapterEmployeeType;
    private SpinnerAdapter adapterCastType;
    private SpinnerAdapter adapterCriteria;
    private SpinnerAdapter adapterReligion;

    private ArrayList<String> arlEmployeeType = new ArrayList<>();
    private ArrayList<String> arlCastType = new ArrayList<>();
    private ArrayList<String> arlCriteria = new ArrayList<>();
    private ArrayList<String> arlReligion = new ArrayList<>();

    private ImageView ivHouseImage1, ivHouseImage2, ivHouseImage3, ivUser;

    private String imageName;
    private byte[] byteArrayImage;
    private String image_profile = "";
    private int setHouseImage=0;
    private String houseImage1="", houseImage2="", houseImage3="", userImage="";

    private Context context;
    private CCIISession cciiSession;

    private String employmentType ="", castType="", criteria="", religion = "";
    private RadioButton rbYesRationCard, rbNoRationCard, rbYesWaterArrangement, rbNoWaterArrangement, rbYesHouse, rbNoHouse, rbYesToilet, rbNoToilet,
            rbYesElectricity, rbNoElectricity, rbYesTax,rbNoTax, rbYesLoan, rbNoLoan, rbYesVoterIdentityCard, rbNoVoterIdentityCard, rbYesGovtJobCard, rbNoLoanGovtJobCard,
            rbYesAgriculturalLand, rbNoAgriculturalLand, rbYesCourtCase, rbNoCourtCase, rbYesDomesticAnimal, rbNoDomesticAnimal, rbYesMedicalPlanning, rbNoMedicalPlanning,
            rbYesTenanted,rbNoTenanted;

    private AlertDialog gpsAlert;

    private double latitude=0;
    private double longitude=0;

    private String otpCode="";

    private String surveyId="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_entry);
        context = EditSurveyEntryActivity.this;
        cciiSession = new CCIISession(context);
        getIds();
        setListenerrs();
        setSpinnerData();

        if(getIntent().getStringExtra("surveyId")!=null && getIntent().getStringExtra("surveyId").length()>0){
            surveyId = getIntent().getStringExtra("surveyId");
        }

        getSurveyDetail();

    }

    private void getIds() {
        etHouseHolder = (EditText) findViewById(R.id.etHouseHolder);
        etAdharNumber = (EditText) findViewById(R.id.etAdharNumber);
        etMuncipalHoldingNo = (EditText) findViewById(R.id.etMuncipalHoldingNo);
        etVoterListSerialNo = (EditText) findViewById(R.id.etVoterListSerialNo);
        etHouseNumber = (EditText) findViewById(R.id.etHouseNumber);
        etAreaofLivingHouse = (EditText) findViewById(R.id.etAreaofLivingHouse);
        etUnemployedBoys = (EditText) findViewById(R.id.etUnemployedBoys);
        etUnemployedGirls = (EditText) findViewById(R.id.etUnemployedGirls);
        etMemberofTenanted = (EditText) findViewById(R.id.etMemberofTenanted);
        etFullAddress = (EditText) findViewById(R.id.etFullAddress);
        etPinCode = (EditText) findViewById(R.id.etPinCode);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        ivHouseImage1 = (ImageView) findViewById(R.id.ivHouseImage1);
        ivHouseImage2 = (ImageView) findViewById(R.id.ivHouseImage2);
        ivHouseImage3 = (ImageView) findViewById(R.id.ivHouseImage3);
        ivUser = (ImageView) findViewById(R.id.ivUser);

        tvSubmit = (TextView) findViewById(R.id.tvSubmit);;
        tvPrint = (TextView) findViewById(R.id.tvPrint);;
        tvPrint.setVisibility(View.VISIBLE);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        spinnerEmployeeType = (Spinner) findViewById(R.id.spinnerEmployeeType);
        spinnerCastType = (Spinner) findViewById(R.id.spinnerCastType);
        spinnerCriteria = (Spinner) findViewById(R.id.spinnerCriteria);
        spinnerReligion = (Spinner) findViewById(R.id.spinnerReligion);

        rbYesRationCard = (RadioButton) findViewById(R.id.rbYesRationCard);
        rbNoRationCard = (RadioButton) findViewById(R.id.rbNoRationCard);
        rbYesWaterArrangement = (RadioButton) findViewById(R.id.rbYesWaterArrangement);
        rbNoWaterArrangement = (RadioButton) findViewById(R.id.rbNoWaterArrangement);
        rbYesHouse = (RadioButton) findViewById(R.id.rbYesHouse);
        rbNoHouse = (RadioButton) findViewById(R.id.rbNoHouse);
        rbYesToilet = (RadioButton) findViewById(R.id.rbYesToilet);
        rbNoToilet = (RadioButton) findViewById(R.id.rbNoToilet);
        rbYesElectricity = (RadioButton) findViewById(R.id.rbYesElectricity);
        rbNoElectricity = (RadioButton) findViewById(R.id.rbNoElectricity);
        rbYesTax = (RadioButton) findViewById(R.id.rbYesTax);
        rbNoTax = (RadioButton) findViewById(R.id.rbNoTax);
        rbYesLoan = (RadioButton) findViewById(R.id.rbYesLoan);
        rbNoLoan = (RadioButton) findViewById(R.id.rbNoLoan);
        rbYesVoterIdentityCard = (RadioButton) findViewById(R.id.rbYesVoterIdentityCard);
        rbNoVoterIdentityCard = (RadioButton) findViewById(R.id.rbNoVoterIdentityCard);
        rbYesGovtJobCard = (RadioButton) findViewById(R.id.rbYesGovtJobCard);
        rbNoLoanGovtJobCard = (RadioButton) findViewById(R.id.rbNoLoanGovtJobCard);
        rbYesAgriculturalLand = (RadioButton) findViewById(R.id.rbYesAgriculturalLand);
        rbNoAgriculturalLand = (RadioButton) findViewById(R.id.rbNoAgriculturalLand);
        rbYesCourtCase = (RadioButton) findViewById(R.id.rbYesCourtCase);
        rbNoCourtCase = (RadioButton) findViewById(R.id.rbNoCourtCase);
        rbYesDomesticAnimal = (RadioButton) findViewById(R.id.rbYesDomesticAnimal);
        rbNoDomesticAnimal = (RadioButton) findViewById(R.id.rbNoDomesticAnimal);
        rbYesMedicalPlanning = (RadioButton) findViewById(R.id.rbYesMedicalPlanning);
        rbNoMedicalPlanning = (RadioButton) findViewById(R.id.rbNoMedicalPlanning);
        rbYesTenanted = (RadioButton) findViewById(R.id.rbYesTenanted);
        rbNoTenanted = (RadioButton) findViewById(R.id.rbNoTenanted);

    }

    private void setListenerrs() {
        tvSubmit.setOnClickListener(this);
        */
/*ivHouseImage1.setOnClickListener(this);
        ivHouseImage2.setOnClickListener(this);
        ivHouseImage3.setOnClickListener(this);
        ivUser.setOnClickListener(this);*//*

    }

    private void setSpinnerData() {
        arlEmployeeType.add("Business");
        arlEmployeeType.add("Employee ");
        arlEmployeeType.add("Other");
        arlEmployeeType.add("Farmer");
        adapterEmployeeType = new SpinnerAdapter(EditSurveyEntryActivity.this, arlEmployeeType);
        spinnerEmployeeType.setAdapter(adapterEmployeeType);

        arlCastType.add("S.T");
        arlCastType.add("S.C");
        arlCastType.add("O.B.C");
        arlCastType.add("OTHERS");
        adapterCastType = new SpinnerAdapter(EditSurveyEntryActivity.this, arlCastType);
        spinnerCastType.setAdapter(adapterCastType);

        arlCriteria.add("A.P.L");
        arlCriteria.add("B.P.L");
        arlCriteria.add("OTHERS");
        adapterCriteria = new SpinnerAdapter(EditSurveyEntryActivity.this, arlCriteria);
        spinnerCriteria.setAdapter(adapterCriteria);


        arlReligion.add("Hindu");
        arlReligion.add("Muslim");
        arlReligion.add("Others");
        adapterReligion = new SpinnerAdapter(EditSurveyEntryActivity.this, arlReligion);
        spinnerReligion.setAdapter(adapterReligion);


        spinnerEmployeeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                employmentType = arlEmployeeType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCastType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                castType = arlCastType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerReligion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                religion = arlReligion.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCriteria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                criteria = arlCriteria.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:
                finish();

                */
/*if(isValid()){
                   // finish();
                  //  sendOTPApi();
                }*//*

            break;

            case R.id.tvPrint:

            break;

            */
/*case R.id.ivHouseImage1:
                if(checkPermissionStorage(EditSurveyEntryActivity.this)){
                    if(checkPermissionCamera(EditSurveyEntryActivity.this)){
                        setHouseImage = 1;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(EditSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(EditSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(EditSurveyEntryActivity.this);
                }


                break;

            case R.id.ivHouseImage2:

                if(checkPermissionStorage(EditSurveyEntryActivity.this)){
                    if(checkPermissionCamera(EditSurveyEntryActivity.this)){
                        setHouseImage = 2;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(EditSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(EditSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(EditSurveyEntryActivity.this);
                }



                break;

            case R.id.ivHouseImage3:
                if(checkPermissionStorage(EditSurveyEntryActivity.this)){
                    if(checkPermissionCamera(EditSurveyEntryActivity.this)){
                        setHouseImage = 3;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(EditSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(EditSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(EditSurveyEntryActivity.this);
                }

                break;

            case R.id.ivUser:
                if(checkPermissionStorage(EditSurveyEntryActivity.this)){
                    if(checkPermissionCamera(EditSurveyEntryActivity.this)){
                        setHouseImage = 4;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(EditSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(EditSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(EditSurveyEntryActivity.this);
                }

                break;*//*


        }
    }

    */
/* this method is used for take picture from camera *//*

    public  void takePicture(Activity activity,  String fileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            mImageCaptureUri = Uri.fromFile(new File(activity.getExternalFilesDir("temp"), fileName+".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TakePictureUtils.TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    */
/** this method is used for open crop image *//*

    public  void startCropImage(Activity activity, String fileName) {
        Intent intent = new Intent(activity, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, new File(activity.getExternalFilesDir("temp") ,fileName).getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 1);
        intent.putExtra(CropImage.ASPECT_Y, 1);
        intent.putExtra(CropImage.OUTPUT_X, 200);
        intent.putExtra(CropImage.OUTPUT_Y, 200);
        startActivityForResult(intent, TakePictureUtils.CROP_FROM_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if ( requestCode==  TakePictureUtils.TAKE_PICTURE){
            if(resultCode==Activity.RESULT_OK){
                startCropImage(EditSurveyEntryActivity.this,imageName + ".jpg");
            }
        } else if (requestCode== TakePictureUtils.CROP_FROM_CAMERA){
            if(resultCode==Activity.RESULT_OK){

                String path = null;
                if(intent != null){
                    path = intent.getStringExtra(CropImage.IMAGE_PATH);
                }
                if (path == null) {
                    return;
                }

                if(setHouseImage==1){
                    houseImage1=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   */
/* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*//*

                    ivHouseImage1.setImageBitmap(bm);

                }else if(setHouseImage==2){
                    houseImage2=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage2);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   */
/* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*//*

                    ivHouseImage2.setImageBitmap(bm);
                }else if(setHouseImage==4){
                    userImage=path;
                    Bitmap bm = BitmapFactory.decodeFile(userImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   */
/* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*//*

                    //ivUser.setImageBitmap(bm);

                    Picasso.with(EditSurveyEntryActivity.this).load(new File(userImage)).error(R.drawable.user_placeholder).transform(new CircleTransformation()).into(ivUser);

                }else{
                    houseImage3=path;

                    Bitmap bm = BitmapFactory.decodeFile(houseImage3);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   */
/* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*//*

                    ivHouseImage3.setImageBitmap(bm);
                }

            }

        }
    }

    private boolean isValid()
    {
        if(etHouseHolder.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Name of House Holder.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Mobile Number.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()<10){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Mobile Number.");
            return false;
        }else if(etPinCode.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area Pin Code.");
            return false;
        }else if(etHouseNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter House Number.");
            return false;
        }else if(etFullAddress.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Full Address.");
            return false;
        }else if(etAreaofLivingHouse.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area of Living House.");
            return false;

        }else if(etUnemployedBoys.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Unemployed Person Boys.");
            return false;
        }else if(etUnemployedGirls.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Unemployed Person Girls.");
            return false;
        }else if(etMemberofTenanted.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Member of Tenanted.");
            return false;
        }else{
            return true;
        }
    }




    public static  boolean checkPermissionStorage(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }


        } else {
            return false;

        }
    }

    public static boolean checkPermissionCamera(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result  == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }



    public static   void requestPermissionStorage( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)||ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }

    public static  void requestPermissionCamera(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }



    private void showOTPDialog(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_otp, null);
        mDialog.setContentView(dialoglayout);

        final EditText etOtp = (EditText)mDialog.findViewById(R.id.etOtp);
        TextView tvSubmit = (TextView)mDialog.findViewById(R.id.tvSubmit);
        TextView tvCancel = (TextView)mDialog.findViewById(R.id.tvCancel);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(etOtp.getText().toString().trim().length()>0){
                    if(etOtp.getText().toString().trim().equalsIgnoreCase(otpCode)){
                        mDialog.dismiss();


                    }else{
                        try {
                            CommonUtils.setSnackbar(llMainView, "Please Enter Valid OTP.");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    try {
                        CommonUtils.setSnackbar(llMainView, "Please Enter OTP.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
            }
        });



        mDialog.show();
    }

    private void sendOTPApi(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email",etMobileNumber.getText().toString().trim());
            jsonObject.addProperty("code","");

            String url = RequestURL.OTP;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("201")){
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(jsonResponse.code!=null && jsonResponse.code.length()>0){
                                        otpCode = jsonResponse.code;
                                    }

                                    showOTPDialog(EditSurveyEntryActivity.this);

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermission()){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!statusOfGPS){
                showGPSAlert();
            }else{
                setUpdatedLocation();
            }
        }else {
            requestPermission();
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(EditSurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }



    private void requestPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(EditSurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(EditSurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(EditSurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(llMainView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(llMainView,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    */
/** this is method is used for display gps open alert *//*

    private void showGPSAlert(){
        if(gpsAlert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Gps is disable please enable it")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                            startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            gpsAlert = builder.create();
            gpsAlert.show();
        }else{
            try {
                if(!gpsAlert.isShowing()){
                    gpsAlert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpdatedLocation(){
        com.ccii.utils.LocationTracker locationTracker = new com.ccii.utils.LocationTracker(EditSurveyEntryActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("","latitude "+ latitude + "longitude"+ longitude);

                }


            }
        });
        locationTracker.onUpdateLocation();
    }


    private void getSurveyDetail(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();

            String url = RequestURL.SUREY_DETAIL+"survey_id="+surveyId;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){

                                    if(jsonResponse.survey!=null){
                                        setInfo(jsonResponse.survey);
                                    }



                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }

    }

    private void setInfo(SurveyDetailData survey) {

        if(survey.house_holder_name!=null && survey.house_holder_name.length()>0){
            etHouseHolder.setText(survey.house_holder_name);
        }

        if(survey.adhaar_no!=null && survey.adhaar_no.length()>0){
            etAdharNumber.setText(survey.adhaar_no);
        }
        if(survey.munciple_holding_no!=null && survey.munciple_holding_no.length()>0){
            etMuncipalHoldingNo.setText(survey.munciple_holding_no);
        }
        if(survey.voter_list_serial_no!=null && survey.voter_list_serial_no.length()>0){
            etVoterListSerialNo.setText(survey.voter_list_serial_no);
        }
        if(survey.house_no!=null && survey.house_no.length()>0){
            etHouseNumber.setText(survey.house_no);
        }

        if(survey.employee_type!=null && survey.employee_type.length()>0){
           if(arlEmployeeType!=null && arlEmployeeType.size()>0){
               for (int i = 0; i < arlEmployeeType.size(); i++) {
                   if(arlEmployeeType.get(i).equalsIgnoreCase(survey.employee_type)){
                       spinnerEmployeeType.setSelection(i);
                   }
               }
           }
        }

        if(survey.ration_card!=null && survey.ration_card.equalsIgnoreCase("1")){
            rbYesRationCard.setChecked(true);
        }else{
            rbNoRationCard.setChecked(true);
        }

        if(survey.water_arrangment!=null && survey.water_arrangment.equalsIgnoreCase("1")){
            rbYesWaterArrangement.setChecked(true);
        }else{
            rbNoWaterArrangement.setChecked(true);
        }

        if(survey.house_pakka_kacha!=null && survey.house_pakka_kacha.equalsIgnoreCase("1")){
            rbYesHouse.setChecked(true);
        }else{
            rbNoHouse.setChecked(true);
        }

        if(survey.toilet_pakka_kacha!=null && survey.toilet_pakka_kacha.equalsIgnoreCase("1")){
            rbYesToilet.setChecked(true);
        }else{
            rbNoToilet.setChecked(true);
        }

        if(survey.elctricity!=null && survey.elctricity.equalsIgnoreCase("1")){
            rbYesElectricity.setChecked(true);
        }else{
            rbNoElectricity.setChecked(true);
        }

        if(survey.elctricity!=null && survey.elctricity.equalsIgnoreCase("1")){
            rbYesElectricity.setChecked(true);
        }else{
            rbNoElectricity.setChecked(true);
        }


        if(survey.cast_type!=null && survey.cast_type.length()>0){
            if(arlCastType!=null && arlCastType.size()>0){
                for (int i = 0; i < arlCastType.size(); i++) {
                    if(arlCastType.get(i).equalsIgnoreCase(survey.cast_type)){
                        spinnerCastType.setSelection(i);
                    }
                }
            }
        }

        if(survey.criteria!=null && survey.criteria.length()>0){
            if(arlCriteria!=null && arlCriteria.size()>0){
                for (int i = 0; i < arlCriteria.size(); i++) {
                    if(arlCriteria.get(i).equalsIgnoreCase(survey.criteria)){
                        spinnerCriteria.setSelection(i);
                    }
                }
            }
        }

        if(survey.religion!=null && survey.religion.length()>0){
            if(arlReligion!=null && arlReligion.size()>0){
                for (int i = 0; i < arlReligion.size(); i++) {
                    if(arlReligion.get(i).equalsIgnoreCase(survey.religion)){
                        spinnerReligion.setSelection(i);
                    }
                }
            }
        }

        if(survey.panchayat_munciple_tax!=null && survey.panchayat_munciple_tax.equalsIgnoreCase("1")){
            rbYesTax.setChecked(true);
        }else{
            rbNoTax.setChecked(true);
        }

        if(survey.govt_loan!=null && survey.govt_loan.equalsIgnoreCase("1")){
            rbYesLoan.setChecked(true);
        }else{
            rbNoLoan.setChecked(true);
        }

        if(survey.voter_identity_card!=null && survey.voter_identity_card.equalsIgnoreCase("1")){
            rbYesVoterIdentityCard.setChecked(true);
        }else{
            rbNoVoterIdentityCard.setChecked(true);
        }

        if(survey.govt_job_card!=null && survey.govt_job_card.equalsIgnoreCase("1")){
            rbYesGovtJobCard.setChecked(true);
        }else{
            rbNoLoanGovtJobCard.setChecked(true);
        }

        if(survey.agricultural_land!=null && survey.agricultural_land.equalsIgnoreCase("1")){
            rbYesAgriculturalLand.setChecked(true);
        }else{
            rbNoAgriculturalLand.setChecked(true);
        }

        if(survey.area_of_living_sqrft!=null && survey.area_of_living_sqrft.length()>0){
            etAreaofLivingHouse.setText(survey.area_of_living_sqrft);
        }

        if(survey.unemployed_person_boys!=null && survey.unemployed_person_boys.length()>0){
            etUnemployedBoys.setText(survey.unemployed_person_boys);
        }

        if(survey.unemployed_person_girls!=null && survey.unemployed_person_girls.length()>0){
            etUnemployedGirls.setText(survey.unemployed_person_girls);
        }

        if(survey.court_case!=null && survey.court_case.equalsIgnoreCase("1")){
            rbYesCourtCase.setChecked(true);
        }else{
            rbNoCourtCase.setChecked(true);
        }

        if(survey.domestic_animal!=null && survey.domestic_animal.equalsIgnoreCase("1")){
            rbYesDomesticAnimal.setChecked(true);
        }else{
            rbNoDomesticAnimal.setChecked(true);
        }

        if(survey.medical_facility!=null && survey.medical_facility.equalsIgnoreCase("1")){
            rbYesMedicalPlanning.setChecked(true);
        }else{
            rbNoMedicalPlanning.setChecked(true);
        }

        if(survey.tentanted!=null && survey.tentanted.equalsIgnoreCase("1")){
            rbYesTenanted.setChecked(true);
        }else{
            rbNoTenanted.setChecked(true);
        }

        if(survey.mobile_no!=null && survey.mobile_no.length()>0){
            etMobileNumber.setText(survey.mobile_no);
        }

        if(survey.pin_code!=null && survey.pin_code.length()>0){
            etPinCode.setText(survey.pin_code);
        }

        if(survey.full_address!=null && survey.full_address.length()>0){
            etFullAddress.setText(survey.full_address);
        }

        if(survey.member_of_tentanted!=null && survey.member_of_tentanted.length()>0){
            etMemberofTenanted.setText(survey.member_of_tentanted);
        }

        if(survey.house_image1!=null && survey.house_image1.length()>0){
           Picasso.with(EditSurveyEntryActivity.this).load(survey.house_image1).error(R.drawable.house_placeholer).resize(200,200).into(ivHouseImage1);
        }

        if(survey.house_image2!=null && survey.house_image2.length()>0){
           Picasso.with(EditSurveyEntryActivity.this).load(survey.house_image2).error(R.drawable.house_placeholer).resize(200,200).into(ivHouseImage2);
        }

        if(survey.house_image3!=null && survey.house_image3.length()>0){
           Picasso.with(EditSurveyEntryActivity.this).load(survey.house_image3).error(R.drawable.house_placeholer).resize(200,200).into(ivHouseImage3);
        }

        if(survey.user_image!=null && survey.user_image.length()>0){
            Picasso.with(EditSurveyEntryActivity.this).load(survey.user_image).error(R.drawable.user_placeholder).resize(200,200).into(ivUser);
        }
    }

}
*/
