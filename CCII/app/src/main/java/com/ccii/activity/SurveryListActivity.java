package com.ccii.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.ccii.R;
import com.ccii.adapter.SurveryListAdapter;
import com.ccii.model.SurveyListData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class SurveryListActivity extends Activity  {

    private LinearLayout llMainView;
    private RecyclerView rvSurveyList;
    private RecyclerView.LayoutManager mLayoutManager;
    private SurveryListAdapter adapter;

    private String fromDate = "", toDate="";
    private Context context;
    private ArrayList<SurveyListData> arlSurveryList;
    private CCIISession cciiSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survery_list);
        context = SurveryListActivity.this;
        arlSurveryList = new ArrayList<SurveyListData>();
        cciiSession = new CCIISession(SurveryListActivity.this);
        getIds();
        setListenerrs();


        if(getIntent().getStringExtra("fromDate")!=null && getIntent().getStringExtra("fromDate").length()>0){
            fromDate = getIntent().getStringExtra("fromDate")+"%2000:00:01";
        }

        if(getIntent().getStringExtra("toDate")!=null && getIntent().getStringExtra("toDate").length()>0){
            toDate = getIntent().getStringExtra("toDate")+"%2023:59:59";
        }


        getSurveryListAPI();


    }

    private void getIds() {
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        rvSurveyList = (RecyclerView) findViewById(R.id.rvSurveyList);
        rvSurveyList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(SurveryListActivity.this);
        rvSurveyList.setLayoutManager(mLayoutManager);

        adapter = new SurveryListAdapter(SurveryListActivity.this, arlSurveryList);
        rvSurveyList.setAdapter(adapter);

    }

    private void setListenerrs() {

    }


    private void getSurveryListAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();

           // String url = RequestURL.SUREY_LIST +"start_date="+fromDate+"&&end_date="+toDate+" 23:59:59"+"&&user_id="+cciiSession.getUserId();
          //  String url = RequestURL.SUREY_LIST +"start_date="+fromDate+ " 00:00:01"+"&&end_date="+toDate+" 23:59:59"+"&&user_id="+cciiSession.getUserId();

            String url = RequestURL.SUREY_LIST +"start_date="+fromDate+"&&end_date="+toDate+"&&user_id="+cciiSession.getUserId();

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){

                                    if(jsonResponse.surveys!=null && jsonResponse.surveys.size()>0){
                                        arlSurveryList.addAll(jsonResponse.surveys);
                                        adapter.notifyDataSetChanged();
                                        saveAppJsonDataPrefs(SurveryListActivity.this,arlSurveryList);
                                    }
                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }

    }
    public void saveAppJsonDataPrefs(Context context,ArrayList<SurveyListData> JsonString){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(JsonString);
        editor.putString("MyObject", json);
        editor.commit();
    }
}
