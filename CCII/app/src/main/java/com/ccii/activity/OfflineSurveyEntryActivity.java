package com.ccii.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.adapter.SpinnerAdapter;
import com.ccii.cropimage.CropImage;
import com.ccii.database.SurveyEntryTable;
import com.ccii.model.RequestSurveyEntryData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CircleTransformation;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.ccii.utils.LocationResult;
import com.ccii.utils.TakePictureUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class OfflineSurveyEntryActivity extends Activity implements View.OnClickListener {

    private TextView tvSubmit, tvPrint;
    private EditText etHouseHolder,etAdharNumber, etMuncipalHoldingNo,etVoterListSerialNo, etMobileNumber, etPinCode,
            etHouseNumber, etFullAddress, etAreaofLivingHouse, etMemberofTenanted,
            etTotalMinorMembers, etTotalAdultsMember, etTotalFamilyMember, etPanchayatMunicipalTax;
    private LinearLayout llMainView;

    private Spinner spinnerEmployeeType, spinnerCastType, spinnerCriteria, spinnerReligion, spinnerHouseType;
    private SpinnerAdapter adapterEmployeeType;
    private SpinnerAdapter adapterCastType;
    private SpinnerAdapter adapterCriteria;
    private SpinnerAdapter adapterReligion;
    private SpinnerAdapter adapterHouseType;

    private ArrayList<String> arlEmployeeType = new ArrayList<>();
    private ArrayList<String> arlCastType = new ArrayList<>();
    private ArrayList<String> arlCriteria = new ArrayList<>();
    private ArrayList<String> arlReligion = new ArrayList<>();
    private ArrayList<String> arlHouseType = new ArrayList<>();

    private ImageView ivHouseImage1, ivHouseImage2, ivHouseImage3, ivUser;

    private String imageName;
    private byte[] byteArrayImage;
    private String image_profile = "";
    private int setHouseImage=0;
    private String houseImage1="", houseImage2="", houseImage3="", userImage="";

    private Context context;
    private CCIISession cciiSession;

    private String employmentType ="", castType="", criteria="", religion = "", houseType="";
    private RadioButton rbYesRationCard, rbNoRationCard, rbYesWaterArrangement, rbNoWaterArrangement, rbYesHouse, rbNoHouse, rbYesToilet, rbNoToilet,
            rbYesElectricity, rbNoElectricity, rbYesTax,rbNoTax, rbYesLoan, rbNoLoan, rbYesVoterIdentityCard, rbNoVoterIdentityCard, rbYesGovtJobCard, rbNoLoanGovtJobCard,
            rbYesAgriculturalLand, rbNoAgriculturalLand, rbYesCourtCase, rbNoCourtCase, rbYesDomesticAnimal, rbNoDomesticAnimal, rbYesMedicalPlanning, rbNoMedicalPlanning,
            rbYesTenanted,rbNoTenanted, rbYesPayment, rbNoPayment;

   private AlertDialog gpsAlert;
    private AlertDialog alert;

    private double latitude=0;
    private double longitude=0;

    private String otpCode="";
    private String surveyId="";

    private TextView tvHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_entry);
        context = OfflineSurveyEntryActivity.this;
        cciiSession = new CCIISession(context);
        getIds();
        setListenerrs();
        setSpinnerData();

    }

    private void getIds() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        etHouseHolder = (EditText) findViewById(R.id.etHouseHolder);
        etAdharNumber = (EditText) findViewById(R.id.etAdharNumber);
        etMuncipalHoldingNo = (EditText) findViewById(R.id.etMuncipalHoldingNo);
        etVoterListSerialNo = (EditText) findViewById(R.id.etVoterListSerialNo);
        etHouseNumber = (EditText) findViewById(R.id.etHouseNumber);
        etAreaofLivingHouse = (EditText) findViewById(R.id.etAreaofLivingHouse);
        etTotalMinorMembers = (EditText) findViewById(R.id.etTotalMinorMembers);
        etTotalAdultsMember = (EditText) findViewById(R.id.etTotalAdultsMember);
        etTotalFamilyMember = (EditText) findViewById(R.id.etTotalFamilyMember);
        etMemberofTenanted = (EditText) findViewById(R.id.etMemberofTenanted);
        etFullAddress = (EditText) findViewById(R.id.etFullAddress);
        etPinCode = (EditText) findViewById(R.id.etPinCode);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etPanchayatMunicipalTax = (EditText) findViewById(R.id.etPanchayatMunicipalTax);
        ivHouseImage1 = (ImageView) findViewById(R.id.ivHouseImage1);
        ivHouseImage2 = (ImageView) findViewById(R.id.ivHouseImage2);
        ivHouseImage3 = (ImageView) findViewById(R.id.ivHouseImage3);
        ivUser = (ImageView) findViewById(R.id.ivUser);

        tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        tvPrint = (TextView) findViewById(R.id.tvPrint);
        tvPrint.setVisibility(View.GONE);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        spinnerEmployeeType = (Spinner) findViewById(R.id.spinnerEmployeeType);
        spinnerCastType = (Spinner) findViewById(R.id.spinnerCastType);
        spinnerCriteria = (Spinner) findViewById(R.id.spinnerCriteria);
        spinnerReligion = (Spinner) findViewById(R.id.spinnerReligion);
        spinnerHouseType = (Spinner) findViewById(R.id.spinnerHouseType);



        rbYesPayment = (RadioButton) findViewById(R.id.rbYesPayment);
        rbNoPayment = (RadioButton) findViewById(R.id.rbNoPayment);

        rbYesRationCard = (RadioButton) findViewById(R.id.rbYesRationCard);
        rbNoRationCard = (RadioButton) findViewById(R.id.rbNoRationCard);
        rbYesWaterArrangement = (RadioButton) findViewById(R.id.rbYesWaterArrangement);
        rbNoWaterArrangement = (RadioButton) findViewById(R.id.rbNoWaterArrangement);
        rbYesHouse = (RadioButton) findViewById(R.id.rbYesHouse);
        rbNoHouse = (RadioButton) findViewById(R.id.rbNoHouse);
        rbYesToilet = (RadioButton) findViewById(R.id.rbYesToilet);
        rbNoToilet = (RadioButton) findViewById(R.id.rbNoToilet);
        rbYesElectricity = (RadioButton) findViewById(R.id.rbYesElectricity);
        rbNoElectricity = (RadioButton) findViewById(R.id.rbNoElectricity);
        rbYesTax = (RadioButton) findViewById(R.id.rbYesTax);
        rbNoTax = (RadioButton) findViewById(R.id.rbNoTax);
        rbYesLoan = (RadioButton) findViewById(R.id.rbYesLoan);
        rbNoLoan = (RadioButton) findViewById(R.id.rbNoLoan);
        rbYesVoterIdentityCard = (RadioButton) findViewById(R.id.rbYesVoterIdentityCard);
        rbNoVoterIdentityCard = (RadioButton) findViewById(R.id.rbNoVoterIdentityCard);
        rbYesGovtJobCard = (RadioButton) findViewById(R.id.rbYesGovtJobCard);
        rbNoLoanGovtJobCard = (RadioButton) findViewById(R.id.rbNoLoanGovtJobCard);
        rbYesAgriculturalLand = (RadioButton) findViewById(R.id.rbYesAgriculturalLand);
        rbYesCourtCase = (RadioButton) findViewById(R.id.rbYesCourtCase);
        rbNoCourtCase = (RadioButton) findViewById(R.id.rbNoCourtCase);
        rbYesDomesticAnimal = (RadioButton) findViewById(R.id.rbYesDomesticAnimal);
        rbNoDomesticAnimal = (RadioButton) findViewById(R.id.rbNoDomesticAnimal);
        rbYesMedicalPlanning = (RadioButton) findViewById(R.id.rbYesMedicalPlanning);
        rbNoMedicalPlanning = (RadioButton) findViewById(R.id.rbNoMedicalPlanning);
        rbYesTenanted = (RadioButton) findViewById(R.id.rbYesTenanted);
        rbNoTenanted = (RadioButton) findViewById(R.id.rbNoTenanted);

    }

    private void setListenerrs() {
        tvSubmit.setOnClickListener(this);
        ivHouseImage1.setOnClickListener(this);
        ivHouseImage2.setOnClickListener(this);
        ivHouseImage3.setOnClickListener(this);
        ivUser.setOnClickListener(this);
    }

    private void setSpinnerData() {
        arlEmployeeType.add("Business");
        arlEmployeeType.add("Employee ");
        arlEmployeeType.add("Other");
        arlEmployeeType.add("Farmer");
        adapterEmployeeType = new SpinnerAdapter(OfflineSurveyEntryActivity.this, arlEmployeeType);
        spinnerEmployeeType.setAdapter(adapterEmployeeType);

        arlCastType.add("S.T");
        arlCastType.add("S.C");
        arlCastType.add("O.B.C");
        arlCastType.add("General");
        arlCastType.add("OTHERS");
        adapterCastType = new SpinnerAdapter(OfflineSurveyEntryActivity.this, arlCastType);
        spinnerCastType.setAdapter(adapterCastType);

        arlCriteria.add("A.P.L");
        arlCriteria.add("B.P.L");
        arlCriteria.add("OTHERS");
        adapterCriteria = new SpinnerAdapter(OfflineSurveyEntryActivity.this, arlCriteria);
        spinnerCriteria.setAdapter(adapterCriteria);


        arlReligion.add("Hindu");
        arlReligion.add("Muslim");
        arlReligion.add("Others");
        adapterReligion = new SpinnerAdapter(OfflineSurveyEntryActivity.this, arlReligion);
        spinnerReligion.setAdapter(adapterReligion);

        arlHouseType.add("Owned");
        arlHouseType.add("Tenant");
        adapterHouseType = new SpinnerAdapter(OfflineSurveyEntryActivity.this, arlHouseType);
        spinnerHouseType.setAdapter(adapterHouseType);


        spinnerHouseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                houseType = arlHouseType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEmployeeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                employmentType = arlEmployeeType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCastType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                castType = arlCastType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerReligion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                religion = arlReligion.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCriteria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                criteria = arlCriteria.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:
                if(isValid()){
                    saveDataForDB();
                }

            break;

            case R.id.ivHouseImage1:
                if(checkPermissionStorage(OfflineSurveyEntryActivity.this)){
                    if(checkPermissionCamera(OfflineSurveyEntryActivity.this)){
                        setHouseImage = 1;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(OfflineSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(OfflineSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(OfflineSurveyEntryActivity.this);
                }


                break;

            case R.id.ivHouseImage2:

                if(checkPermissionStorage(OfflineSurveyEntryActivity.this)){
                    if(checkPermissionCamera(OfflineSurveyEntryActivity.this)){
                        setHouseImage = 2;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(OfflineSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(OfflineSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(OfflineSurveyEntryActivity.this);
                }



                break;

            case R.id.ivHouseImage3:
                if(checkPermissionStorage(OfflineSurveyEntryActivity.this)){
                    if(checkPermissionCamera(OfflineSurveyEntryActivity.this)){
                        setHouseImage = 3;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(OfflineSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(OfflineSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(OfflineSurveyEntryActivity.this);
                }

                break;

            case R.id.ivUser:
                if(checkPermissionStorage(OfflineSurveyEntryActivity.this)){
                    if(checkPermissionCamera(OfflineSurveyEntryActivity.this)){
                        setHouseImage = 4;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(OfflineSurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(OfflineSurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(OfflineSurveyEntryActivity.this);
                }

                break;

        }
    }

    /* this method is used for take picture from camera */
    public  void takePicture(Activity activity,  String fileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            mImageCaptureUri = Uri.fromFile(new File(activity.getExternalFilesDir("temp"), fileName+".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TakePictureUtils.TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** this method is used for open crop image */
    public  void startCropImage(Activity activity, String fileName) {
        Intent intent = new Intent(activity, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, new File(activity.getExternalFilesDir("temp") ,fileName).getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 1);
        intent.putExtra(CropImage.ASPECT_Y, 1);
        intent.putExtra(CropImage.OUTPUT_X, 200);
        intent.putExtra(CropImage.OUTPUT_Y, 200);
        startActivityForResult(intent, TakePictureUtils.CROP_FROM_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if ( requestCode==  TakePictureUtils.TAKE_PICTURE){
            if(resultCode==Activity.RESULT_OK){
                startCropImage(OfflineSurveyEntryActivity.this,imageName + ".jpg");
            }
        } else if (requestCode== TakePictureUtils.CROP_FROM_CAMERA){
            if(resultCode==Activity.RESULT_OK){

                String path = null;
                if(intent != null){
                    path = intent.getStringExtra(CropImage.IMAGE_PATH);
                }
                if (path == null) {
                    return;
                }

                if(setHouseImage==1){
                    houseImage1=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage1.setImageBitmap(bm);

                }else if(setHouseImage==2){
                    houseImage2=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage2);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage2.setImageBitmap(bm);
                }else if(setHouseImage==4){
                    userImage=path;
                    Bitmap bm = BitmapFactory.decodeFile(userImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    //ivUser.setImageBitmap(bm);

                    Picasso.with(OfflineSurveyEntryActivity.this).load(new File(userImage)).error(R.drawable.user_placeholder).transform(new CircleTransformation()).into(ivUser);
                    etHouseHolder.requestFocus();
                    etHouseHolder.setSelection(etHouseHolder.getText().toString().trim().length());

                }else{
                    houseImage3=path;

                    Bitmap bm = BitmapFactory.decodeFile(houseImage3);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage3.setImageBitmap(bm);
                }

            }

        }
    }

    private boolean isValid()
    {
        if(etHouseHolder.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Name of House Holder.");
            return false;
        }else if(userImage!=null && userImage.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Upload House Holder Image.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Mobile Number.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()<10){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Mobile Number.");
            return false;
        }else if(etPinCode.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area Pin Code.");
            return false;
        }else if(etHouseNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter House Number.");
            return false;
        }else if(etFullAddress.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Full Address.");
            return false;
        }else if(etPanchayatMunicipalTax.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Panchayat Municipal Tax.");
            return false;

        }else if(etAreaofLivingHouse.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area of Living House.");
            return false;

        }else if(etTotalMinorMembers.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Minor Members.");
            return false;
        }else if(etTotalAdultsMember.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Adults Member.");
            return false;
        }else if(etTotalFamilyMember.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Family Member.");
            return false;
        }/*else if(etMemberofTenanted.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Member of Tenanted.");
            return false;
        }*/else{
            return true;
        }
    }






    public static  boolean checkPermissionStorage(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }


        } else {
            return false;

        }
    }

    public static boolean checkPermissionCamera(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result  == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }



    public static   void requestPermissionStorage( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)||ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }

    public static  void requestPermissionCamera(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }

    private void saveDataForDB(){

        RequestSurveyEntryData requestSurveyEntryData = new RequestSurveyEntryData();
        requestSurveyEntryData.user_id=cciiSession.getUserId();
        requestSurveyEntryData.house_holder_name=etHouseHolder.getText().toString().trim();
       requestSurveyEntryData.adhaar_no=etAdharNumber.getText().toString().trim();
       requestSurveyEntryData.munciple_holding_no=etMuncipalHoldingNo.getText().toString().trim();
       requestSurveyEntryData.voter_list_serial_no=etVoterListSerialNo.getText().toString().trim();
       requestSurveyEntryData.house_no=etHouseNumber.getText().toString().trim();
       requestSurveyEntryData.employee_type= employmentType;

        if(rbYesPayment.isChecked()){
           requestSurveyEntryData.payment="1";
        }else{
           requestSurveyEntryData.payment="0";
        }

        if(rbYesRationCard.isChecked()){
           requestSurveyEntryData.ration_card="1";
        }else{
           requestSurveyEntryData.ration_card="0";
        }
        if(rbYesWaterArrangement.isChecked()){
           requestSurveyEntryData.water_arrangement="1";
        }else{
           requestSurveyEntryData.water_arrangement="0";
        }

        if(rbYesHouse.isChecked()){
           requestSurveyEntryData.house_pakka_kacha="1";
        }else{
           requestSurveyEntryData.house_pakka_kacha="0";
        }

        if(rbYesToilet.isChecked()){
           requestSurveyEntryData.toilet_pakka_kacha="1";
        }else{
           requestSurveyEntryData.toilet_pakka_kacha="0";
        }

        if(rbYesElectricity.isChecked()){
           requestSurveyEntryData.elctricity="1";
        }else{
           requestSurveyEntryData.elctricity="0";
        }

       requestSurveyEntryData.cast_type=castType;
       requestSurveyEntryData.criteria=criteria;
       requestSurveyEntryData.religion=religion;

       requestSurveyEntryData.panchayat_munciple_tax=etPanchayatMunicipalTax.getText().toString().trim();
       requestSurveyEntryData.house_type= houseType;

         /*   if(rbYesTax.isChecked()){
               requestSurveyEntryData.panchayat_munciple_tax="1");
            }else{
               requestSurveyEntryData.panchayat_munciple_tax="0");
            }*/

        if(rbYesLoan.isChecked()){
           requestSurveyEntryData.govt_loan="1";
        }else{
           requestSurveyEntryData.govt_loan="0";
        }

        if(rbYesVoterIdentityCard.isChecked()){
           requestSurveyEntryData.voter_identity_card="1";
        }else{
           requestSurveyEntryData.voter_identity_card="0";
        }

        if(rbYesGovtJobCard.isChecked()){
           requestSurveyEntryData.govt_job_card="1";
        }else{
           requestSurveyEntryData.govt_job_card="0";
        }

        if(rbYesAgriculturalLand.isChecked()){
           requestSurveyEntryData.agricultural_land="1";
        }else{
           requestSurveyEntryData.agricultural_land="0";
        }


       requestSurveyEntryData.area_of_living_sqrft=etAreaofLivingHouse.getText().toString().trim();
       requestSurveyEntryData.members=etTotalFamilyMember.getText().toString().trim();
       requestSurveyEntryData.adults=etTotalAdultsMember.getText().toString().trim();
       requestSurveyEntryData.minors=etTotalMinorMembers.getText().toString().trim();

        if(rbYesCourtCase.isChecked()){
           requestSurveyEntryData.court_case="1";
        }else{
           requestSurveyEntryData.court_case="0";
        }

        if(rbYesDomesticAnimal.isChecked()){
           requestSurveyEntryData.domestic_animal="1";
        }else{
           requestSurveyEntryData.domestic_animal="0";
        }

        if(rbYesMedicalPlanning.isChecked()){
           requestSurveyEntryData.medical_facility="1";
        }else{
           requestSurveyEntryData.medical_facility="0";
        }

        if(rbYesTenanted.isChecked()){
           requestSurveyEntryData.tentanted="1";
        }else{
           requestSurveyEntryData.tentanted="0";
        }

       requestSurveyEntryData.member_of_tentanted=etMemberofTenanted.getText().toString().trim();
       requestSurveyEntryData.lat=String.valueOf(latitude);
       requestSurveyEntryData.lng=String.valueOf(longitude);

       requestSurveyEntryData.mobile_no=etMobileNumber.getText().toString().trim();
       requestSurveyEntryData.pin_code=etPinCode.getText().toString().trim();
       requestSurveyEntryData.full_address=etFullAddress.getText().toString().trim();

       requestSurveyEntryData.profile_image=userImage;
       requestSurveyEntryData.house_image1=houseImage1;
       requestSurveyEntryData.house_image2=houseImage2;
       requestSurveyEntryData.house_image3=houseImage3;

        try {
            SurveyEntryTable surveyEntryTable = new SurveyEntryTable(OfflineSurveyEntryActivity.this);
            surveyEntryTable.insertToTable(requestSurveyEntryData, cciiSession.getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        CommonUtils.showToast(OfflineSurveyEntryActivity.this, "Data saved in Local DB.");
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermission()){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!statusOfGPS){
                showGPSAlert();
            }else{
                setUpdatedLocation();
            }
        }else {
            requestPermission();
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(OfflineSurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }



    private void requestPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(OfflineSurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(OfflineSurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(OfflineSurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(llMainView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(llMainView,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    /** this is method is used for display gps open alert */
    private void showGPSAlert(){
        if(gpsAlert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Gps is disable please enable it")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                            startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            gpsAlert = builder.create();
            gpsAlert.show();
        }else{
            try {
                if(!gpsAlert.isShowing()){
                    gpsAlert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpdatedLocation(){
        com.ccii.utils.LocationTracker locationTracker = new com.ccii.utils.LocationTracker(OfflineSurveyEntryActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("","latitude "+ latitude + "longitude"+ longitude);

                }


            }
        });
        locationTracker.onUpdateLocation();
    }

    public static String encodeTobase64(Bitmap image)
    {
        String encodedImage = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArrayImage = baos.toByteArray();
            encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
            Log.d("base64Encode", encodedImage);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedImage;
    }

    private void showAlert(String msg){
        if(alert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });
                   /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });*/
            alert = builder.create();
            alert.show();
        }else{
            try {
                if(!alert.isShowing()){
                    alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
