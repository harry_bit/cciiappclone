package com.ccii.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.utils.CCIISession;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class SplashActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    private CCIISession cciiSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        cciiSession = new CCIISession(this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(cciiSession.getIsLogin() && cciiSession.getUserId().length()>0){
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }else{
                    showCustomDialog(SplashActivity.this);
                }
            }
        }, SPLASH_TIME_OUT);
    }


    private void showCustomDialog(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_splash, null);
        mDialog.setContentView(dialoglayout);
         ImageView ivOk = (ImageView)mDialog.findViewById(R.id.ivOk);
        final RadioButton rbNewUser = (RadioButton)mDialog.findViewById(R.id.rbNewUser);
        final RadioButton rbExistingUser = (RadioButton)mDialog.findViewById(R.id.rbExistingUser);

        ivOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rbNewUser.isChecked()){
                    mDialog.dismiss();
                    Intent intent = new Intent(SplashActivity.this, SignupActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();

                }else if(rbExistingUser.isChecked()){
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }
            }
        });



        mDialog.show();
    }
}
