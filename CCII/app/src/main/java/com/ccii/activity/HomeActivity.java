package com.ccii.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.database.SurveyEntryTable;
import com.ccii.model.RequestSurveyEntryData;
import com.ccii.syncdata.DailyReportSyncTask;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi Shah on 6/19/2016.
 */
public class HomeActivity extends Activity implements View.OnClickListener {

    private TextView tvSurveyDataEntry,tvSurveyDataSearch, tvMapRoute, tvLogout, tvSurveyDataEntryOffline, tvSyncData;
    private LinearLayout llMainView;
    private CCIISession cciiSession;

    private List<RequestSurveyEntryData> arlSurveyEntry;
    private AlertDialog alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        cciiSession = new CCIISession(HomeActivity.this);
        arlSurveyEntry = new ArrayList<>();
        getIds();
        setListenerrs();

    }


    private void getIds() {
        tvSurveyDataEntry = (TextView) findViewById(R.id.tvSurveyDataEntry);
        tvSyncData = (TextView) findViewById(R.id.tvSyncData);
        tvSurveyDataSearch = (TextView) findViewById(R.id.tvSurveyDataSearch);
        tvMapRoute = (TextView) findViewById(R.id.tvMapRoute);
        tvLogout = (TextView) findViewById(R.id.tvLogout);
        tvSurveyDataEntryOffline = (TextView) findViewById(R.id.tvSurveyDataEntryOffline);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
    }

    private void setListenerrs() {
        tvMapRoute.setOnClickListener(this);
        tvSurveyDataSearch.setOnClickListener(this);
        tvSurveyDataEntry.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        tvSurveyDataEntryOffline.setOnClickListener(this);
        tvSyncData.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvMapRoute:
                Intent intentMap = new Intent(HomeActivity.this, RouteMapActivity.class);
                startActivity(intentMap);
            break;

            case R.id.tvSurveyDataSearch:
                    Intent intent = new Intent(HomeActivity.this, SearchSurveryActivity.class);
                    startActivity(intent);
            break;

            case R.id.tvSyncData:
                if (CommonUtils.isNetworkAvailable(HomeActivity.this)) {
                    syncData();
                }else{
                        CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

                }

            break;

            case R.id.tvSurveyDataEntryOffline:
                    Intent intent1 = new Intent(HomeActivity.this, OfflineSurveyEntryActivity.class);
                    startActivity(intent1);
            break;

            case R.id.tvSurveyDataEntry:
                    Intent intentEntry = new Intent(HomeActivity.this, SurveyEntryActivity.class);
                    startActivity(intentEntry);
            break;

            case R.id.tvLogout:
                logOutDialog("Are you sure want to Logout?");
            break;
        }
    }


    private void logOutDialog(final String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        cciiSession.setSession("", "", false);
                        CommonUtils.savePreferencesString(HomeActivity.this, Constants.SURVEY_ID_VISIBLE, "");
                        CommonUtils.savePreferencesString(HomeActivity.this,"MyObject","");
                        Intent intentLogout = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(intentLogout);
                        finish();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void syncData(){
        List<RequestSurveyEntryData> arlSurveyEntryList = new ArrayList<>();

        try {
            SurveyEntryTable surveyEntryTable = new SurveyEntryTable(HomeActivity.this);
            arlSurveyEntryList.addAll(surveyEntryTable.getAllSurveyEntryList(cciiSession.getUserId()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(arlSurveyEntryList!=null && arlSurveyEntryList.size()>0){
            DailyReportSyncTask dailyReportSyncTask = new DailyReportSyncTask(HomeActivity.this, arlSurveyEntryList, llMainView);
            dailyReportSyncTask.execute();
        }else{
            CommonUtils.setSnackbar(llMainView, "No data found.");
        }


    }

}
