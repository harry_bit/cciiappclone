package com.ccii.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ccii.R;
import com.ccii.location.DirectionsJSONParser;
import com.ccii.location.LocationTracker;
import com.ccii.location.MapDisplayUtils;
import com.ccii.location.RouteParser;
import com.ccii.location.Routes;
import com.ccii.model.GeoCodeJSONParser;
import com.ccii.model.SurveyListData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.ccii.utils.LocationResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class RouteMapActivity extends Activity implements View.OnClickListener {

    private LinearLayout llMainView;
    private EditText etSearch;
    private AutoCompleteTextView etIDSearch;
    private GoogleMap googleMap;
    private MapFragment fragmentManager;
    private AlertDialog gpsAlert;

    private double latitude=0;
    private double currentLatitude=0;
    private double longitude=0;
    private double currentLongitude=0;

    private LocationTracker driverLocationTracker;
    private ArrayList<LatLng> paths= new ArrayList<LatLng>();
    private MapDisplayUtils mapDisplayUtils;

    private List<LatLng> list;
    private RouteParser routeParsers=null;
    private List<Routes> finalRoute;
    private LatLng dropupCoordinates;
    private Activity mContext;
    private Geocoder geocoder;

    private List<HashMap<String, String>> geoCode = null;
    private List<SurveyListData>arlSurveyList;

    private String  TAG = RouteMapActivity.class.getSimpleName();
    private String surveyID="";

    private Context context;
    private List<Address> addresses;
    private ImageView ivMyLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_route);
        context = RouteMapActivity.this;
        getIds();
        mContext = RouteMapActivity.this;
        mapDisplayUtils= new MapDisplayUtils();
        //dropupCoordinates = new LatLng(28.6139, 77.2090);
        setListenerrs();
    }

    private void getIds() {

        ivMyLocation = (ImageView) findViewById(R.id.ivMyLocation);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        etSearch = (EditText) findViewById(R.id.etSearch);
        etIDSearch = (AutoCompleteTextView) findViewById(R.id.etIDSearch);
        fragmentManager = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        fragmentManager.getView().setVisibility(View.INVISIBLE);

        arlSurveyList=new ArrayList<SurveyListData>();
        arlSurveyList=getAppJsonDataPrefs(RouteMapActivity.this);
        if(CommonUtils.getPreferencesString(context,Constants.SURVEY_ID_VISIBLE)!=null){
            etIDSearch.setText(CommonUtils.getPreferencesString(context,Constants.SURVEY_ID_VISIBLE));
        }


        if(arlSurveyList!=null&&arlSurveyList.size()>0) {
            String[] arSyrveyID = new String[arlSurveyList.size()];
            for (int i = 0; i < arlSurveyList.size(); i++) {
                arSyrveyID[i] = arlSurveyList.get(i).main_survey_id;
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arSyrveyID);
            etIDSearch.setAdapter(adapter);

        }
    }

    private void setListenerrs() {
        ivMyLocation.setOnClickListener(this);

       /* etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                CommonUtils.hideSoftKeyboard(RouteMapActivity.this);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.isNetworkAvailable(RouteMapActivity.this)) {
                        if (etSearch.getText().toString().trim().length() > 0) {
                            Log.e("", "Call Place Api setOnEditorActionListener .");
                            GeoCodeTask placesTask = new GeoCodeTask(etSearch.getText().toString().trim());
                            placesTask.execute();
                        }


                    } else {
                        Toast.makeText(RouteMapActivity.this, R.string.no_network_connection, Toast.LENGTH_LONG).show();
                    }
                }
                return true;
            }
        });*/

        etIDSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                CommonUtils.hideSoftKeyboard(RouteMapActivity.this);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (etIDSearch.getText().toString().trim().length() > 0) {

                        for(int i=0;i<arlSurveyList.size();i++){
                            if(arlSurveyList.get(i).main_survey_id.equals(etIDSearch.getText().toString().trim())){
                                surveyID=arlSurveyList.get(i).survey_id;
                                break;
                            }
                        }

                    System.out.println("SURVEY ID : "+surveyID+" List Size : "+arlSurveyList.size());

                        getSurveyDetail();
                    }
                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ivMyLocation:
                setUpdatedCurrentLocation();

                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermission()){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!statusOfGPS){
                showGPSAlert();
            }else{
                setUpdatedLocation();
            }
        }else {
            requestPermission();
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(RouteMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }



    private void requestPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(RouteMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(RouteMapActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(RouteMapActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(llMainView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(llMainView,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    /** this is method is used for display gps open alert */
    private void showGPSAlert(){
        if(gpsAlert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Gps is disable please enable it")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                            startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            gpsAlert = builder.create();
            gpsAlert.show();
        }else{
            try {
                if(!gpsAlert.isShowing()){
                    gpsAlert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpdatedLocation(){
        com.ccii.utils.LocationTracker locationTracker = new com.ccii.utils.LocationTracker(RouteMapActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    if(currentLatitude<=0){
                        currentLatitude = location.getLatitude();
                    }

                    if(currentLongitude<=0){
                        currentLongitude = location.getLongitude();
                    }

                    try {
                        Log.e("","currentLatitude "+ currentLatitude + "currentLongitude"+ currentLongitude);
                        new GetLocationAsync(currentLatitude, currentLongitude).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Log.e("","latitude "+ latitude + "longitude"+ longitude);


                    try {
                        initilizeMap();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });
        locationTracker.onUpdateLocation();
    }


    private void initilizeMap() {
        LinearLayout llMapView = (LinearLayout)findViewById(R.id.llMapView);

        if (googleMap == null) {

            fragmentManager.getView().setVisibility(View.INVISIBLE);

            googleMap = (fragmentManager).getMap();


            if (googleMap != null) {

                MapDisplayUtils.animateToMeters(2000, RouteMapActivity.this, googleMap, new LatLng(latitude, longitude), llMapView.getWidth());
                googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin)));

                fragmentManager.getView().setVisibility(View.VISIBLE);


            }
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void startLocationTracking(){
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(statusOfGPS){
            updateDriverLocation();
        }else
        {
            Toast.makeText(RouteMapActivity.this,"Please enable GPS for getting accurate location",Toast.LENGTH_SHORT).show();
        }
    }

    private void updateDriverLocation(){
        driverLocationTracker = new LocationTracker(RouteMapActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        updateMap(latitude,longitude);

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }
        });
        driverLocationTracker.onUpdateLocation();
    }

    private void updateMap(double latitude,double longitude) {

        LinearLayout llMapView = (LinearLayout)findViewById(R.id.llMapView);

        if(googleMap!=null) {
            googleMap.clear();
                paths.add(new LatLng(latitude, longitude));
                try {
                    googleMap.clear();

                    mapDisplayUtils.showRouteMap(paths, googleMap);

                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(getDirectionsUrl(new LatLng(latitude, longitude), dropupCoordinates));
                   // String ployline = mapDisplayUtils.encode(paths);
                } catch (Exception e) {
                    e.printStackTrace();
                }



        }

    }

    //Creating Polyline in the Map

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";
        String alternatives="alternatives=true";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+alternatives;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;


        url =url.replaceAll(" ", "%20");

        Log.e("", "url >>> "+ url);

        return url;
    }



    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, RouteParser> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                CommonUtils.showProgressDialog(context, "Route Loading...");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Downloading data in non-ui thread
        @Override
        protected RouteParser doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            JSONObject jObject;
            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);

                jObject = new JSONObject(data);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routeParsers = parser.getRouteDetails(jObject.toString());


            }catch(Exception e){
                Log.d("Background Task", e.toString());
            }
            return routeParsers;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(RouteParser result) {
            super.onPostExecute(result);
            if(result!=null){
                try {

                    try {
                        CommonUtils.disMissProgressDialog(mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    finalRoute=  result.routes;
                    showRouteMap(finalRoute, 0);

                    if(googleMap!=null){
                        setMap(new LatLng(latitude, longitude), dropupCoordinates, googleMap);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }



    private void showRouteMap(List<Routes> routes, int position) throws Exception{


        PolylineOptions lineOptions  = new PolylineOptions();
        //encodePath=routes.get(position).overview_polyline.points;
        list= decodePoly(routes.get(position).overview_polyline.points);
        //Log.e("","polyline >>"+ routes.get(position).overview_polyline.points);

        //ApplicationDriverApp.setPolyline(routes.get(position).overview_polyline.points!=null?routes.get(position).overview_polyline.points:"");

        lineOptions.addAll(list);
        lineOptions.width(10);
        lineOptions.color(Color.RED);
        googleMap.addPolyline(lineOptions);
        LatLng startLatLng = new LatLng(routes.get(0).legs.get(0).start_location.lat,routes.get(0).legs.get(0).start_location.lng);
        LatLng endLatLng = new LatLng(routes.get(0).legs.get(0).end_location.lat,routes.get(0).legs.get(0).end_location.lng);

        // Creating MarkerOptions
        MarkerOptions startOptions = new MarkerOptions();
        MarkerOptions endOptions = new MarkerOptions();

        // Setting the position of the marker
        startOptions.position(startLatLng);
        endOptions.position(endLatLng);

        startOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).title("Start");
        endOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("End");
        //startOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin)).title("Start");
        //endOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin)).title("End");

        // Add new marker to the Google Map Android API V2
        googleMap.addMarker(startOptions);
        googleMap.addMarker(endOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(startLatLng));

    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    private class GeoCodeTask extends AsyncTask<String, Void, Object> {
        String address;

        public GeoCodeTask(String address) {
            this.address = address;
        }

        @Override
        protected Object doInBackground(String... place) {
            // For storing data from web service
            Log.e(TAG, "In doInBackground GeoCodeTask." + address);
            String data;
            String strValue = address.replaceAll(" ", "%20");
            Log.e(TAG, "doInBackground:" + strValue);
            // Building the url to the web service

            String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + strValue + "&sensor=true" + "&key=AIzaSyBT9u0xC13kfamydaY98Sc7WuJ-YDEfA6Y";

            Log.e(TAG, "GeoCode url:" + url);
            JSONObject jObject = null;
            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);

                GeoCodeJSONParser geoCodeJsonParser = new GeoCodeJSONParser();
                jObject = new JSONObject(data);

                // Getting the parsed data as a List construct
                geoCode = geoCodeJsonParser.parse(jObject);

            } catch (Exception e) {
                CommonUtils.disMissProgressDialog(mContext);
                e.printStackTrace();
            }
            return jObject;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            Log.e(TAG, "In onPostExecute:" + result);

            if (result != null) {
                JSONObject parsedResult = (JSONObject) result;

                try {
                    if (parsedResult.getString("status") != null) {
                        String status = parsedResult.getString("status");
                        Log.e(TAG, "status:" + status);
                        if (status.equalsIgnoreCase("OK")) {
                            JSONArray jsonArray = parsedResult.getJSONArray("results");
                            if (jsonArray.length() > 0) {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                JSONObject jsonGeometry = jsonObject.getJSONObject("geometry");
                                JSONObject location = jsonGeometry.getJSONObject("location");
                                Double latitude = Double.valueOf(location.getString("lat"));
                                Double longitude = Double.valueOf(location.getString("lng"));

                                dropupCoordinates = new LatLng(latitude, longitude);

                                startLocationTracking();
                            }

                        } else if (status.equalsIgnoreCase("ZERO_RESULTS")) {
                            CommonUtils.hideSoftKeyboard(mContext);
                            Toast.makeText(mContext, "No result found.!", Toast.LENGTH_LONG).show();

                        } else if (status.equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                            CommonUtils.hideSoftKeyboard(mContext);
                            Toast.makeText(mContext, "You are over your quota.!", Toast.LENGTH_LONG).show();

                        } else if (status.equalsIgnoreCase("REQUEST_DENIED")) {
                            CommonUtils.hideSoftKeyboard(mContext);
                            Toast.makeText(mContext, "Request was denied.!", Toast.LENGTH_LONG).show();

                        } else if (status.equalsIgnoreCase("INVALID_REQUEST")) {
                            CommonUtils.hideSoftKeyboard(mContext);
                            Toast.makeText(mContext, "An invalid type was given.!", Toast.LENGTH_LONG).show();

                        } else if (status.equalsIgnoreCase("UNKNOWN_ERROR")) {
                            CommonUtils.hideSoftKeyboard(mContext);
                            Toast.makeText(mContext, "Request could not be processed due to a server error.!", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getSurveyDetail(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();

            //String url = RequestURL.SUREY_DETAIL+"survey_id="+etIDSearch.getText().toString().trim();
            String url = RequestURL.SUREY_DETAIL+"survey_id="+surveyID;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){
                                    if(jsonResponse.survey!=null){

                                      /*  if(jsonResponse.survey.full_address!=null && jsonResponse.survey.full_address.length()>0){
                                            etSearch.setText(jsonResponse.survey.full_address);
                                        }*/

                                        if(jsonResponse.survey.lat!=null && jsonResponse.survey.lat.length()>0 && jsonResponse.survey.lng!=null && jsonResponse.survey.lng.length()>0){
                                            Double latitude = Double.valueOf(jsonResponse.survey.lat);
                                            Double longitude = Double.valueOf(jsonResponse.survey.lng);
                                            dropupCoordinates = new LatLng(latitude, longitude);
                                            startLocationTracking();
                                        }
                                    }
                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }

    }


    private class GetLocationAsync extends AsyncTask<String, Void, String> {

        private double x, y;
        private StringBuilder str;

        public GetLocationAsync(double latitude, double longitude) {
            x = latitude;
            y = longitude;
        }

        @Override
        protected void onPreExecute() {

            Log.e("","x "+ x + "y"+ y);
            etSearch.setText("");
            etSearch.setHint(" Getting location ..");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                geocoder = new Geocoder(RouteMapActivity.this, Locale.ENGLISH);
                addresses = geocoder.getFromLocation(x, y, 1);
                str = new StringBuilder();
                if (geocoder.isPresent()) {
                    Address returnAddress = addresses.get(0);
                    String localityString = returnAddress.getLocality();
                    String city = returnAddress.getCountryName();
                    String region_code = returnAddress.getCountryCode();
                    String zipcode = returnAddress.getPostalCode();
                    str.append(localityString + "");
                    str.append(city + "" + region_code + "");
                    str.append(zipcode + "");

                } else {
                }
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                etSearch.setText(addresses.get(0).getAddressLine(0)+" "
                        + addresses.get(0).getAddressLine(1) + " ");

                Log.e("","address "+ addresses.get(0).getAddressLine(0)+" "
                        + addresses.get(0).getAddressLine(1) + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }


private void setMap(LatLng startAddress, LatLng destinationAddress, final GoogleMap googleMap){

    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    builder.include(startAddress);
    builder.include(destinationAddress);
    LatLngBounds bounds = builder.build();
    final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
    googleMap.animateCamera(cu, new GoogleMap.CancelableCallback(){
        public void onCancel(){}
        public void onFinish(){
            googleMap.animateCamera(cu);
        }
    });
}

    private void setUpdatedCurrentLocation(){
        try {
            LocationTracker locationTracker = new LocationTracker(RouteMapActivity.this, new LocationResult() {
                @Override
                public void gotLocation(Location location) {

                    if(location!=null){

                        double   currentLatitude = location.getLatitude();
                        double currentLongitude = location.getLongitude();

                        if(googleMap!=null){
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 14f));
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude)).zoom(14f).build();
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                }
            });
            locationTracker.onUpdateLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static  List<SurveyListData> getAppJsonDataPrefs(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString("MyObject","");
        Type type = new TypeToken<ArrayList<SurveyListData>>() {}.getType();
        ArrayList<SurveyListData> arrayList = gson.fromJson(json, type);

        return arrayList;
    }
}
