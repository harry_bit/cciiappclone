package com.ccii.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.adapter.SpinnerAdapter;
import com.ccii.cropimage.CropImage;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CircleTransformation;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.ccii.utils.LocationResult;
import com.ccii.utils.TakePictureUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class SurveyEntryActivity extends Activity implements View.OnClickListener {

    private TextView tvSubmit, tvPrint;
    private EditText etHouseHolder,etAdharNumber, etMuncipalHoldingNo,etVoterListSerialNo, etMobileNumber, etPinCode,
            etHouseNumber, etFullAddress, etAreaofLivingHouse, etMemberofTenanted,
            etTotalMinorMembers, etTotalAdultsMember, etTotalFamilyMember, etPanchayatMunicipalTax;
    private LinearLayout llMainView;

    private Spinner spinnerEmployeeType, spinnerCastType, spinnerCriteria, spinnerReligion, spinnerHouseType;
    private SpinnerAdapter adapterEmployeeType;
    private SpinnerAdapter adapterCastType;
    private SpinnerAdapter adapterCriteria;
    private SpinnerAdapter adapterReligion;
    private SpinnerAdapter adapterHouseType;

    private ArrayList<String> arlEmployeeType = new ArrayList<>();
    private ArrayList<String> arlCastType = new ArrayList<>();
    private ArrayList<String> arlCriteria = new ArrayList<>();
    private ArrayList<String> arlReligion = new ArrayList<>();
    private ArrayList<String> arlHouseType = new ArrayList<>();

    private ImageView ivHouseImage1, ivHouseImage2, ivHouseImage3, ivUser;

    private String imageName;
    private byte[] byteArrayImage;
    private String image_profile = "";
    private int setHouseImage=0;
    private String houseImage1="", houseImage2="", houseImage3="", userImage="";

    private Context context;
    private CCIISession cciiSession;

    private String employmentType ="", castType="", criteria="", religion = "", houseType="";
    private RadioButton rbYesRationCard, rbNoRationCard, rbYesWaterArrangement, rbNoWaterArrangement, rbYesHouse, rbNoHouse, rbYesToilet, rbNoToilet,
            rbYesElectricity, rbNoElectricity, rbYesTax,rbNoTax, rbYesLoan, rbNoLoan, rbYesVoterIdentityCard, rbNoVoterIdentityCard, rbYesGovtJobCard, rbNoLoanGovtJobCard,
            rbYesAgriculturalLand, rbNoAgriculturalLand, rbYesCourtCase, rbNoCourtCase, rbYesDomesticAnimal, rbNoDomesticAnimal, rbYesMedicalPlanning, rbNoMedicalPlanning,
            rbYesTenanted,rbNoTenanted, rbYesPayment, rbNoPayment;

   private AlertDialog gpsAlert;
    private AlertDialog alert;

    private double latitude=0;
    private double longitude=0;

    private String otpCode="";
    private String surveyId="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_entry);
        context = SurveyEntryActivity.this;
        cciiSession = new CCIISession(context);
        getIds();
        setListenerrs();
        setSpinnerData();

    }

    private void getIds() {
        etHouseHolder = (EditText) findViewById(R.id.etHouseHolder);
        etAdharNumber = (EditText) findViewById(R.id.etAdharNumber);
        etMuncipalHoldingNo = (EditText) findViewById(R.id.etMuncipalHoldingNo);
        etVoterListSerialNo = (EditText) findViewById(R.id.etVoterListSerialNo);
        etHouseNumber = (EditText) findViewById(R.id.etHouseNumber);
        etAreaofLivingHouse = (EditText) findViewById(R.id.etAreaofLivingHouse);
        etTotalMinorMembers = (EditText) findViewById(R.id.etTotalMinorMembers);
        etTotalAdultsMember = (EditText) findViewById(R.id.etTotalAdultsMember);
        etTotalFamilyMember = (EditText) findViewById(R.id.etTotalFamilyMember);
        etMemberofTenanted = (EditText) findViewById(R.id.etMemberofTenanted);
        etFullAddress = (EditText) findViewById(R.id.etFullAddress);
        etPinCode = (EditText) findViewById(R.id.etPinCode);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etPanchayatMunicipalTax = (EditText) findViewById(R.id.etPanchayatMunicipalTax);
        ivHouseImage1 = (ImageView) findViewById(R.id.ivHouseImage1);
        ivHouseImage2 = (ImageView) findViewById(R.id.ivHouseImage2);
        ivHouseImage3 = (ImageView) findViewById(R.id.ivHouseImage3);
        ivUser = (ImageView) findViewById(R.id.ivUser);

        tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        tvPrint = (TextView) findViewById(R.id.tvPrint);
        tvPrint.setVisibility(View.GONE);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        spinnerEmployeeType = (Spinner) findViewById(R.id.spinnerEmployeeType);
        spinnerCastType = (Spinner) findViewById(R.id.spinnerCastType);
        spinnerCriteria = (Spinner) findViewById(R.id.spinnerCriteria);
        spinnerReligion = (Spinner) findViewById(R.id.spinnerReligion);
        spinnerHouseType = (Spinner) findViewById(R.id.spinnerHouseType);



        rbYesPayment = (RadioButton) findViewById(R.id.rbYesPayment);
        rbNoPayment = (RadioButton) findViewById(R.id.rbNoPayment);

        rbYesRationCard = (RadioButton) findViewById(R.id.rbYesRationCard);
        rbNoRationCard = (RadioButton) findViewById(R.id.rbNoRationCard);
        rbYesWaterArrangement = (RadioButton) findViewById(R.id.rbYesWaterArrangement);
        rbNoWaterArrangement = (RadioButton) findViewById(R.id.rbNoWaterArrangement);
        rbYesHouse = (RadioButton) findViewById(R.id.rbYesHouse);
        rbNoHouse = (RadioButton) findViewById(R.id.rbNoHouse);
        rbYesToilet = (RadioButton) findViewById(R.id.rbYesToilet);
        rbNoToilet = (RadioButton) findViewById(R.id.rbNoToilet);
        rbYesElectricity = (RadioButton) findViewById(R.id.rbYesElectricity);
        rbNoElectricity = (RadioButton) findViewById(R.id.rbNoElectricity);
        rbYesTax = (RadioButton) findViewById(R.id.rbYesTax);
        rbNoTax = (RadioButton) findViewById(R.id.rbNoTax);
        rbYesLoan = (RadioButton) findViewById(R.id.rbYesLoan);
        rbNoLoan = (RadioButton) findViewById(R.id.rbNoLoan);
        rbYesVoterIdentityCard = (RadioButton) findViewById(R.id.rbYesVoterIdentityCard);
        rbNoVoterIdentityCard = (RadioButton) findViewById(R.id.rbNoVoterIdentityCard);
        rbYesGovtJobCard = (RadioButton) findViewById(R.id.rbYesGovtJobCard);
        rbNoLoanGovtJobCard = (RadioButton) findViewById(R.id.rbNoLoanGovtJobCard);
        rbYesAgriculturalLand = (RadioButton) findViewById(R.id.rbYesAgriculturalLand);
        rbYesCourtCase = (RadioButton) findViewById(R.id.rbYesCourtCase);
        rbNoCourtCase = (RadioButton) findViewById(R.id.rbNoCourtCase);
        rbYesDomesticAnimal = (RadioButton) findViewById(R.id.rbYesDomesticAnimal);
        rbNoDomesticAnimal = (RadioButton) findViewById(R.id.rbNoDomesticAnimal);
        rbYesMedicalPlanning = (RadioButton) findViewById(R.id.rbYesMedicalPlanning);
        rbNoMedicalPlanning = (RadioButton) findViewById(R.id.rbNoMedicalPlanning);
        rbYesTenanted = (RadioButton) findViewById(R.id.rbYesTenanted);
        rbNoTenanted = (RadioButton) findViewById(R.id.rbNoTenanted);

    }

    private void setListenerrs() {
        tvSubmit.setOnClickListener(this);
        ivHouseImage1.setOnClickListener(this);
        ivHouseImage2.setOnClickListener(this);
        ivHouseImage3.setOnClickListener(this);
        ivUser.setOnClickListener(this);
    }

    private void setSpinnerData() {
        arlEmployeeType.add("Business");
        arlEmployeeType.add("Employee ");
        arlEmployeeType.add("Other");
        arlEmployeeType.add("Farmer");
        adapterEmployeeType = new SpinnerAdapter(SurveyEntryActivity.this, arlEmployeeType);
        spinnerEmployeeType.setAdapter(adapterEmployeeType);

        arlCastType.add("S.T");
        arlCastType.add("S.C");
        arlCastType.add("O.B.C");
        arlCastType.add("General");
        arlCastType.add("OTHERS");
        adapterCastType = new SpinnerAdapter(SurveyEntryActivity.this, arlCastType);
        spinnerCastType.setAdapter(adapterCastType);

        arlCriteria.add("A.P.L");
        arlCriteria.add("B.P.L");
        arlCriteria.add("OTHERS");
        adapterCriteria = new SpinnerAdapter(SurveyEntryActivity.this, arlCriteria);
        spinnerCriteria.setAdapter(adapterCriteria);


        arlReligion.add("Hindu");
        arlReligion.add("Muslim");
        arlReligion.add("Others");
        adapterReligion = new SpinnerAdapter(SurveyEntryActivity.this, arlReligion);
        spinnerReligion.setAdapter(adapterReligion);

        arlHouseType.add("Owned");
        arlHouseType.add("Tenant");
        adapterHouseType = new SpinnerAdapter(SurveyEntryActivity.this, arlHouseType);
        spinnerHouseType.setAdapter(adapterHouseType);


        spinnerHouseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                houseType = arlHouseType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEmployeeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                employmentType = arlEmployeeType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCastType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                castType = arlCastType.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerReligion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                religion = arlReligion.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCriteria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                criteria = arlCriteria.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:

                if(isValid()){
                   // finish();

                    sendOTPApi();


                }

            break;

            case R.id.ivHouseImage1:
                if(checkPermissionStorage(SurveyEntryActivity.this)){
                    if(checkPermissionCamera(SurveyEntryActivity.this)){
                        setHouseImage = 1;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(SurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(SurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(SurveyEntryActivity.this);
                }


                break;

            case R.id.ivHouseImage2:

                if(checkPermissionStorage(SurveyEntryActivity.this)){
                    if(checkPermissionCamera(SurveyEntryActivity.this)){
                        setHouseImage = 2;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(SurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(SurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(SurveyEntryActivity.this);
                }



                break;

            case R.id.ivHouseImage3:
                if(checkPermissionStorage(SurveyEntryActivity.this)){
                    if(checkPermissionCamera(SurveyEntryActivity.this)){
                        setHouseImage = 3;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(SurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(SurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(SurveyEntryActivity.this);
                }

                break;

            case R.id.ivUser:
                if(checkPermissionStorage(SurveyEntryActivity.this)){
                    if(checkPermissionCamera(SurveyEntryActivity.this)){
                        setHouseImage = 4;
                        imageName = "picture_"+System.currentTimeMillis();
                        takePicture(SurveyEntryActivity.this, imageName);
                    }else {
                        requestPermissionCamera(SurveyEntryActivity.this);
                    }
                }else {
                    requestPermissionStorage(SurveyEntryActivity.this);
                }

                break;

        }
    }

    /* this method is used for take picture from camera */
    public  void takePicture(Activity activity,  String fileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            mImageCaptureUri = Uri.fromFile(new File(activity.getExternalFilesDir("temp"), fileName+".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TakePictureUtils.TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** this method is used for open crop image */
    public  void startCropImage(Activity activity, String fileName) {
        Intent intent = new Intent(activity, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, new File(activity.getExternalFilesDir("temp") ,fileName).getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 1);
        intent.putExtra(CropImage.ASPECT_Y, 1);
        intent.putExtra(CropImage.OUTPUT_X, 200);
        intent.putExtra(CropImage.OUTPUT_Y, 200);
        startActivityForResult(intent, TakePictureUtils.CROP_FROM_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if ( requestCode==  TakePictureUtils.TAKE_PICTURE){
            if(resultCode==Activity.RESULT_OK){
                startCropImage(SurveyEntryActivity.this,imageName + ".jpg");
            }
        } else if (requestCode== TakePictureUtils.CROP_FROM_CAMERA){
            if(resultCode==Activity.RESULT_OK){

                String path = null;
                if(intent != null){
                    path = intent.getStringExtra(CropImage.IMAGE_PATH);
                }
                if (path == null) {
                    return;
                }

                if(setHouseImage==1){
                    houseImage1=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage1.setImageBitmap(bm);

                }else if(setHouseImage==2){
                    houseImage2=path;
                    Bitmap bm = BitmapFactory.decodeFile(houseImage2);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage2.setImageBitmap(bm);
                }else if(setHouseImage==4){
                    userImage=path;
                    Bitmap bm = BitmapFactory.decodeFile(userImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    //ivUser.setImageBitmap(bm);

                    Picasso.with(SurveyEntryActivity.this).load(new File(userImage)).error(R.drawable.user_placeholder).transform(new CircleTransformation()).into(ivUser);
                    etHouseHolder.requestFocus();
                    etHouseHolder.setSelection(etHouseHolder.getText().toString().trim().length());

                }else{
                    houseImage3=path;

                    Bitmap bm = BitmapFactory.decodeFile(houseImage3);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                   /* byteArrayImage = baos.toByteArray();
                    image_profile = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);*/
                    ivHouseImage3.setImageBitmap(bm);
                }

            }

        }
    }

    private boolean isValid()
    {
        if(etHouseHolder.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Name of House Holder.");
            return false;
        }else if(userImage!=null && userImage.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Upload House Holder Image.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Mobile Number.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()<10){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Mobile Number.");
            return false;
        }else if(etPinCode.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area Pin Code.");
            return false;
        }else if(etHouseNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter House Number.");
            return false;
        }else if(etFullAddress.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Full Address.");
            return false;
        }else if(etPanchayatMunicipalTax.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Panchayat Municipal Tax.");
            return false;

        }else if(etAreaofLivingHouse.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Area of Living House.");
            return false;

        }else if(etTotalMinorMembers.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Minor Members.");
            return false;
        }else if(etTotalAdultsMember.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Adults Member.");
            return false;
        }else if(etTotalFamilyMember.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Total Family Member.");
            return false;
        }else if(houseImage1!=null && houseImage1.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Upload House Image 1.");
            return false;
        }
        else if(houseImage2!=null && houseImage2.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Upload House Image 2.");
            return false;
        }else if(houseImage3!=null && houseImage3.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Upload House Image 3.");
            return false;
        }
        /*else if(etMemberofTenanted.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Member of Tenanted.");
            return false;
        }*/else{
            return true;
        }
    }






    public static  boolean checkPermissionStorage(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }


        } else {
            return false;

        }
    }

    public static boolean checkPermissionCamera(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result  == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }



    public static   void requestPermissionStorage( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)||ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestURL.PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }

    public static  void requestPermissionCamera(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, RequestURL.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},RequestURL.PERMISSION_REQUEST_CODE);
        }
    }



    private void showOTPDialog(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_otp, null);
        mDialog.setContentView(dialoglayout);

        final EditText etOtp = (EditText)mDialog.findViewById(R.id.etOtp);
        TextView tvSubmit = (TextView)mDialog.findViewById(R.id.tvSubmit);
        TextView tvCancel = (TextView)mDialog.findViewById(R.id.tvCancel);

        etOtp.setText(otpCode);
        etOtp.setSelection(etOtp.getText().toString().trim().length());

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etOtp.getText().toString().trim().length()>0){
                    if(etOtp.getText().toString().trim().equalsIgnoreCase(otpCode)){
                        mDialog.dismiss();
                        surveryEntryAPI();

                    }else{
                        try {
                            CommonUtils.setSnackbar(llMainView, "Please Enter Valid OTP.");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    try {
                        CommonUtils.setSnackbar(llMainView, "Please Enter OTP.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
            }
        });



        mDialog.show();
    }

    private void sendOTPApi(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mobile",etMobileNumber.getText().toString().trim());

            String url = RequestURL.OTP;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("201")){
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(jsonResponse.code!=null && jsonResponse.code.length()>0){
                                        otpCode = jsonResponse.code;
                                    }

                                    showOTPDialog(SurveyEntryActivity.this);

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }

    private void surveryEntryAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("user_id",cciiSession.getUserId());
            jsonObject.addProperty("house_holder_name",etHouseHolder.getText().toString().trim());
            jsonObject.addProperty("adhaar_no",etAdharNumber.getText().toString().trim());
            jsonObject.addProperty("munciple_holding_no",etMuncipalHoldingNo.getText().toString().trim());
            jsonObject.addProperty("voter_list_serial_no",etVoterListSerialNo.getText().toString().trim());
            jsonObject.addProperty("house_no",etHouseNumber.getText().toString().trim());
            jsonObject.addProperty("employee_type", employmentType);

            if(rbYesPayment.isChecked()){
                jsonObject.addProperty("payment","1");
            }else{
                jsonObject.addProperty("payment","0");
            }

            if(rbYesRationCard.isChecked()){
                jsonObject.addProperty("ration_card","1");
            }else{
                jsonObject.addProperty("ration_card","0");
            }
            if(rbYesWaterArrangement.isChecked()){
                jsonObject.addProperty("water_arrangement","1");
            }else{
                jsonObject.addProperty("water_arrangement","0");
            }

            if(rbYesHouse.isChecked()){
                jsonObject.addProperty("house_pakka_kacha","1");
            }else{
                jsonObject.addProperty("house_pakka_kacha","0");
            }

            if(rbYesToilet.isChecked()){
                jsonObject.addProperty("toilet_pakka_kacha","1");
            }else{
                jsonObject.addProperty("toilet_pakka_kacha","0");
            }

            if(rbYesElectricity.isChecked()){
                jsonObject.addProperty("elctricity","1");
            }else{
                jsonObject.addProperty("elctricity","0");
            }

            jsonObject.addProperty("cast_type",castType);
            jsonObject.addProperty("criteria",criteria);
            jsonObject.addProperty("religion",religion);

            jsonObject.addProperty("panchayat_munciple_tax",etPanchayatMunicipalTax.getText().toString().trim());
            jsonObject.addProperty("house_type", houseType);

         /*   if(rbYesTax.isChecked()){
                jsonObject.addProperty("panchayat_munciple_tax","1");
            }else{
                jsonObject.addProperty("panchayat_munciple_tax","0");
            }*/

            if(rbYesLoan.isChecked()){
                jsonObject.addProperty("govt_loan","1");
            }else{
                jsonObject.addProperty("govt_loan","0");
            }

            if(rbYesVoterIdentityCard.isChecked()){
                jsonObject.addProperty("voter_identity_card","1");
            }else{
                jsonObject.addProperty("voter_identity_card","0");
            }

            if(rbYesGovtJobCard.isChecked()){
                jsonObject.addProperty("govt_job_card","1");
            }else{
                jsonObject.addProperty("govt_job_card","0");
            }

            if(rbYesAgriculturalLand.isChecked()){
                jsonObject.addProperty("agricultural_land","1");
            }else{
                jsonObject.addProperty("agricultural_land","0");
            }


            jsonObject.addProperty("area_of_living_sqrft",etAreaofLivingHouse.getText().toString().trim());
            jsonObject.addProperty("members",etTotalFamilyMember.getText().toString().trim());
            jsonObject.addProperty("adults",etTotalAdultsMember.getText().toString().trim());
            jsonObject.addProperty("minors",etTotalMinorMembers.getText().toString().trim());

            if(rbYesCourtCase.isChecked()){
                jsonObject.addProperty("court_case","1");
            }else{
                jsonObject.addProperty("court_case","0");
            }

            if(rbYesDomesticAnimal.isChecked()){
                jsonObject.addProperty("domestic_animal","1");
            }else{
                jsonObject.addProperty("domestic_animal","0");
            }

            if(rbYesMedicalPlanning.isChecked()){
                jsonObject.addProperty("medical_facility","1");
            }else{
                jsonObject.addProperty("medical_facility","0");
            }

            if(rbYesTenanted.isChecked()){
                jsonObject.addProperty("tentanted","1");
            }else{
                jsonObject.addProperty("tentanted","0");
            }

            jsonObject.addProperty("member_of_tentanted",etMemberofTenanted.getText().toString().trim());
            jsonObject.addProperty("lat",String.valueOf(latitude));
            jsonObject.addProperty("lng",String.valueOf(longitude));

            jsonObject.addProperty("mobile_no",etMobileNumber.getText().toString().trim());
            jsonObject.addProperty("pin_code",etPinCode.getText().toString().trim());
            jsonObject.addProperty("full_address",etFullAddress.getText().toString().trim());


            String url = RequestURL.SURVEY_ENTRY;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("true")){
                                    /*try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/

                                    //For User Image
                                    String base64UserImage = "";
                                    Bitmap bitmapuserImage = null;
                                    File userFile = new File(userImage);
                                    try {
                                        if(userFile!=null && userFile.exists()) {
                                            bitmapuserImage = decodeFile(userFile);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(!CommonUtils.CheckIfNull(bitmapuserImage)){
                                        base64UserImage = encodeTobase64(bitmapuserImage);
                                        Log.e("","base64UserImage "+ base64UserImage);
                                    }else{
                                        base64UserImage = "";
                                    }

                                    if(jsonResponse.survey_id!=null && jsonResponse.survey_id.length()>0){
                                        surveyId = jsonResponse.survey_id;
                                        uploadImageAPI(jsonResponse.survey_id, "user_image", base64UserImage);
                                    }

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }


    private void uploadImageAPI(final String surveyId, String imageType, final String base64) {

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context, "Image uploading...");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("survey_id", surveyId);
            jsonObject.addProperty("image", imageType);
            jsonObject.addProperty("data", base64);

            String url = RequestURL.PHOTO_UPLOAD;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if (jsonResponse != null) {
                                if (jsonResponse.status.equalsIgnoreCase("true")) {

                                    if(houseImage1!=null && houseImage1.length()>0){
                                        //For Home1 Image
                                        String base64Home1Image = "";
                                        Bitmap bitmapHome1Image = null;
                                        File home1File = new File(houseImage1);
                                        try {
                                            if(home1File!=null && home1File.exists()) {
                                                bitmapHome1Image = decodeFile(home1File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome1Image)){
                                            base64Home1Image = encodeTobase64(bitmapHome1Image);
                                            Log.e("","base64Home1Image "+ base64Home1Image);
                                        }else{
                                            base64Home1Image = "";
                                        }

                                        uploadHouseImage1API(surveyId, "house_image1", base64Home1Image);

                                    }else if(houseImage2!=null && houseImage2.length()>0){
//For Home2 Image
                                        String base64Home2Image = "";
                                        Bitmap bitmapHome2Image = null;
                                        File home2File = new File(houseImage2);
                                        try {
                                            if(home2File!=null && home2File.exists()) {
                                                bitmapHome2Image = decodeFile(home2File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome2Image)){
                                            base64Home2Image = encodeTobase64(bitmapHome2Image);
                                            Log.e("","base64Home2Image "+ base64Home2Image);
                                        }else{
                                            base64Home2Image = "";
                                        }

                                        uploadHouseImage2API(surveyId, "house_image2", base64Home2Image);
                                    }else  if(houseImage3!=null && houseImage3.length()>0){
                                        //For Home3 Image
                                        String base64Home3Image = "";
                                        Bitmap bitmapHome3Image = null;
                                        File home3File = new File(houseImage3);
                                        try {
                                            if(home3File!=null && home3File.exists()) {
                                                bitmapHome3Image = decodeFile(home3File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                            base64Home3Image = encodeTobase64(bitmapHome3Image);
                                            Log.e("","base64Home3Image "+ base64Home3Image);
                                        }else{
                                            base64Home3Image = "";
                                        }
                                        uploadHouseImage3API(surveyId, "house_image3", base64Home3Image);
                                    }else {
                                        try {
                                            CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(jsonResponse.message!=null && jsonResponse.message.length()>0){
                                            showAlert(jsonResponse.message);
                                        }else{
                                            showAlert("Successfully uploaded");
                                        }
                                    }

                                } else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }


    private void uploadHouseImage1API(final String surveyId, String imageType, String base64) {

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context, "Image uploading...");
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("survey_id", surveyId);
            jsonObject.addProperty("image", imageType);
            jsonObject.addProperty("data", base64);

            String url = RequestURL.PHOTO_UPLOAD;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if (jsonResponse != null) {
                                if (jsonResponse.status.equalsIgnoreCase("true")) {

                                    if(houseImage2!=null && houseImage2.length()>0){
//For Home2 Image
                                        String base64Home2Image = "";
                                        Bitmap bitmapHome2Image = null;
                                        File home2File = new File(houseImage2);
                                        try {
                                            if(home2File!=null && home2File.exists()) {
                                                bitmapHome2Image = decodeFile(home2File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome2Image)){
                                            base64Home2Image = encodeTobase64(bitmapHome2Image);
                                            Log.e("","base64Home2Image "+ base64Home2Image);
                                        }else{
                                            base64Home2Image = "";
                                        }

                                        uploadHouseImage2API(surveyId, "house_image2", base64Home2Image);
                                    }else  if(houseImage3!=null && houseImage3.length()>0){
                                        //For Home3 Image
                                        String base64Home3Image = "";
                                        Bitmap bitmapHome3Image = null;
                                        File home3File = new File(houseImage3);
                                        try {
                                            if(home3File!=null && home3File.exists()) {
                                                bitmapHome3Image = decodeFile(home3File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                            base64Home3Image = encodeTobase64(bitmapHome3Image);
                                            Log.e("","base64Home3Image "+ base64Home3Image);
                                        }else{
                                            base64Home3Image = "";
                                        }
                                        uploadHouseImage3API(surveyId, "house_image3", base64Home3Image);
                                    }else {
                                        try {
                                            CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if(jsonResponse.message!=null && jsonResponse.message.length()>0){
                                            showAlert(jsonResponse.message);
                                        }else{
                                            showAlert("Successfully uploaded");
                                        }
                                    }
                                } else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }


    private void uploadHouseImage2API(final String surveyId, String imageType, String base64) {

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context, "Image uploading...");
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("survey_id", surveyId);
            jsonObject.addProperty("image", imageType);
            jsonObject.addProperty("data", base64);

            String url = RequestURL.PHOTO_UPLOAD;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if (jsonResponse != null) {
                                if (jsonResponse.status.equalsIgnoreCase("true")) {


                                    if(houseImage3!=null && houseImage3.length()>0){
                                        //For Home3 Image
                                        String base64Home3Image = "";
                                        Bitmap bitmapHome3Image = null;
                                        File home3File = new File(houseImage3);
                                        try {
                                            if(home3File!=null && home3File.exists()) {
                                                bitmapHome3Image = decodeFile(home3File);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(!CommonUtils.CheckIfNull(bitmapHome3Image)){
                                            base64Home3Image = encodeTobase64(bitmapHome3Image);
                                            Log.e("","base64Home3Image "+ base64Home3Image);
                                        }else{
                                            base64Home3Image = "";
                                        }
                                        uploadHouseImage3API(surveyId, "house_image3", base64Home3Image);
                                    }else{
                                        try {
                                            CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(jsonResponse.message!=null && jsonResponse.message.length()>0){
                                            showAlert(jsonResponse.message);
                                        }else{
                                            showAlert("Successfully uploaded");
                                        }
                                    }

                                } else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }


    private void uploadHouseImage3API(String surveyId, String imageType, String base64) {

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context, "Image uploading...");
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("survey_id", surveyId);
            jsonObject.addProperty("image", imageType);
            jsonObject.addProperty("data", base64);

            String url = RequestURL.PHOTO_UPLOAD;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if (jsonResponse != null) {
                                if (jsonResponse.status.equalsIgnoreCase("true")) {

                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(jsonResponse.message!=null && jsonResponse.message.length()>0){
                                        showAlert(jsonResponse.message);
                                    }else{
                                        showAlert("Successfully uploaded");
                                    }

                                } else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }
    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=720;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermission()){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!statusOfGPS){
                showGPSAlert();
            }else{
                setUpdatedLocation();
            }
        }else {
            requestPermission();
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(SurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }



    private void requestPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(SurveyEntryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(SurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(SurveyEntryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(llMainView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(llMainView,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    /** this is method is used for display gps open alert */
    private void showGPSAlert(){
        if(gpsAlert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Gps is disable please enable it")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                            startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            gpsAlert = builder.create();
            gpsAlert.show();
        }else{
            try {
                if(!gpsAlert.isShowing()){
                    gpsAlert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpdatedLocation(){
        com.ccii.utils.LocationTracker locationTracker = new com.ccii.utils.LocationTracker(SurveyEntryActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("","latitude "+ latitude + "longitude"+ longitude);

                }


            }
        });
        locationTracker.onUpdateLocation();
    }

    public static String encodeTobase64(Bitmap image)
    {
        String encodedImage = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArrayImage = baos.toByteArray();
            encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
            Log.d("base64Encode", encodedImage);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedImage;
    }

    private void showAlert(String msg){
        if(alert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });
                   /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });*/
            alert = builder.create();
            alert.show();
        }else{
            try {
                if(!alert.isShowing()){
                    alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
