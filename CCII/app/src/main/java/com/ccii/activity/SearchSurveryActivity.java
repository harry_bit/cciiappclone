package com.ccii.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class SearchSurveryActivity extends Activity implements View.OnClickListener {

    private TextView tvFromDate,tvToDate, tvSearch;
    private LinearLayout llMainView;
    private String startDate,endDate;

    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_survery);
        getIds();
        setListenerrs();

        final Calendar c = Calendar.getInstance();
        mSelectedYear = c.get(Calendar.YEAR);
        mSelectedMonth = c.get(Calendar.MONTH);
        mSelectedDay = c.get(Calendar.DAY_OF_MONTH);
    }

    private void getIds() {
        tvFromDate = (TextView) findViewById(R.id.tvFromDate);
        tvToDate = (TextView) findViewById(R.id.tvToDate);
        tvSearch = (TextView) findViewById(R.id.tvSearch);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
    }

    private void setListenerrs() {
        tvSearch.setOnClickListener(this);
        tvToDate.setOnClickListener(this);
        tvFromDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSearch:
                if(isValid()){
                    Intent intent = new Intent(SearchSurveryActivity.this, SurveryListActivity.class);
                    intent.putExtra("fromDate",startDate.replace(" ","%20"));
                    intent.putExtra("toDate",endDate.replace(" ","%20"));
                    startActivity(intent);
                }
            break;

            case R.id.tvToDate:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListenerTo);
            break;

            case R.id.tvFromDate:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListenerFrom);
            break;
        }
    }

    private boolean isValid()
    {
        if(tvFromDate.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Select From Date.");
            return false;
        }else if(tvToDate.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Select To Date .");
            return false;
        }else{
            return true;
        }
    }


    private DatePickerDialog.OnDateSetListener mOnDateSetListenerTo = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;

            Calendar selectedCal = Calendar.getInstance();
            selectedCal.set(mSelectedYear, mSelectedMonth, mSelectedDay);

            long selectedMilli = selectedCal.getTimeInMillis();
            Date datePickerDate = new Date(selectedMilli);
            if (datePickerDate.after(new Date())) {
                //Toast.makeText(NewOvertimeRequestActivity.this,"Cannot be in the future",Toast.LENGTH_LONG).show();
            }else{
                String month = ((mSelectedMonth + 1) > 9) ? "" + (mSelectedMonth + 1) : "0" + (mSelectedMonth + 1);
                String day = ((mSelectedDay) < 10) ? "0" + mSelectedDay : "" + mSelectedDay;

               tvToDate.setText(mSelectedYear+"-"+month+"-"+day);
                Date d=new Date(datePickerDate.getTime());
                endDate=tvToDate.getText()+" 23:59:59";
            }

        }
    };

    private DatePickerDialog.OnDateSetListener mOnDateSetListenerFrom = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;

            Calendar selectedCal = Calendar.getInstance();
            selectedCal.set(mSelectedYear, mSelectedMonth, mSelectedDay);

            long selectedMilli = selectedCal.getTimeInMillis();
            Date datePickerDate = new Date(selectedMilli);
            if (datePickerDate.after(new Date())) {
                //Toast.makeText(NewOvertimeRequestActivity.this,"Cannot be in the future",Toast.LENGTH_LONG).show();
            }else{
                String month = ((mSelectedMonth + 1) > 9) ? "" + (mSelectedMonth + 1) : "0" + (mSelectedMonth + 1);
                String day = ((mSelectedDay) < 10) ? "0" + mSelectedDay : "" + mSelectedDay;
                tvFromDate.setText(mSelectedYear+"-"+month+"-"+day);
                startDate = tvFromDate.getText()+" 00:01:01";

            }

        }
    };


    // initialize the DatePickerDialog
    private DatePickerDialog showDatePickerDialog(int initialYear, int initialMonth, int initialDay, DatePickerDialog.OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(SearchSurveryActivity.this, listener, initialYear, initialMonth, initialDay);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
        return dialog;
    }
}
