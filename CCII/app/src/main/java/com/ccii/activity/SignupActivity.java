package com.ccii.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.adapter.DistrictSpinnerAdapter;
import com.ccii.adapter.StateSpinnerAdapter;
import com.ccii.adapter.WardSpinnerAdapter;
import com.ccii.adapter.WardTypeSpinnerAdapter;
import com.ccii.responsedata.DistrictData;
import com.ccii.responsedata.StateData;
import com.ccii.responsedata.WardData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.ccii.utils.LocationResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ravi Shah on 6/19/2016.
 */
public class SignupActivity extends Activity implements View.OnClickListener {

    private EditText etName, etMobileNumber, etEmail, etPassword, etConfirmPassword, etLastName;
    private TextView tvDOB, tvSignUp, tvBottomLogin;
    private LinearLayout llMainView;
    private Spinner spinnerState, spinnerDistrict,spinnerWard, spinnerWardType;

    private StateSpinnerAdapter adapterState;
    private DistrictSpinnerAdapter adapterDistrit;
    private WardSpinnerAdapter adapterWard;
    private WardTypeSpinnerAdapter adapterWardType;

    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;
    private Context context;
    private CCIISession cciiSession;

    private ArrayList<StateData> arlState;
    private ArrayList<DistrictData> arlDistrict;
    private ArrayList<WardData> arlWard;
    private ArrayList<String> arlWardType;

    private String selectedStateID="", selectedDistrictID="", selectedWardID="";
    private String selectedState="", selectedDistrict="", selectedWard="", selectedWardType ="";

    private AlertDialog gpsAlert;

    private double latitude=0;
    private double longitude=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = SignupActivity.this;
        cciiSession = new CCIISession(context);
        arlState = new ArrayList<StateData>();
        arlDistrict = new ArrayList<DistrictData>();
        arlWard = new ArrayList<WardData>();
        arlWardType = new ArrayList<String>();
        getIds();
        setListenerrs();
        setSpinnerData();

        final Calendar c = Calendar.getInstance();
        mSelectedYear = c.get(Calendar.YEAR);
        mSelectedMonth = c.get(Calendar.MONTH);
        mSelectedDay = c.get(Calendar.DAY_OF_MONTH);

        getStateAPI();
    }

    private void setSpinnerData() {

        adapterDistrit = new DistrictSpinnerAdapter(SignupActivity.this, arlDistrict);
        spinnerDistrict.setAdapter(adapterDistrit);

        adapterState = new StateSpinnerAdapter(SignupActivity.this, arlState);
        spinnerState.setAdapter(adapterState);

        adapterWard = new WardSpinnerAdapter(SignupActivity.this, arlWard);
        spinnerWard.setAdapter(adapterWard);


        arlWardType.add("Please select");
        arlWardType.add("Mahanagar Palika");
        arlWardType.add("Nagar Parishad");
        arlWardType.add("Gram Panchayat");
        arlWardType.add("Nagar Panchayat");
        adapterWardType = new WardTypeSpinnerAdapter(SignupActivity.this, arlWardType);
        spinnerWardType.setAdapter(adapterWardType);

    }

    private void getIds() {
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etName = (EditText)findViewById(R.id.etName);
        etLastName = (EditText)findViewById(R.id.etLastName);
        etMobileNumber = (EditText)findViewById(R.id.etMobileNumber);
        etConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);

        tvDOB = (TextView) findViewById(R.id.tvDOB);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvBottomLogin = (TextView) findViewById(R.id.tvBottomLogin);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        spinnerState = (Spinner) findViewById(R.id.spinnerState);
        spinnerDistrict = (Spinner) findViewById(R.id.spinnerDistrict);
        spinnerWard = (Spinner) findViewById(R.id.spinnerWard);
        spinnerWardType = (Spinner) findViewById(R.id.spinnerWardType);
    }

    private void setListenerrs() {
        tvSignUp.setOnClickListener(this);
        tvDOB.setOnClickListener(this);
        tvBottomLogin.setOnClickListener(this);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedStateID = arlState.get(position).id;
                selectedState = arlState.get(position).state;
                if(selectedStateID.length()>0){
                    getDistrictAPI();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDistrictID = arlDistrict.get(position).id;
                selectedDistrict = arlDistrict.get(position).district;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedWardID = arlWard.get(position).id;
                selectedWard = arlWard.get(position).ward;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        spinnerWardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0){
                    selectedWardType = arlWardType.get(position);
                    getWardAPI();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSignUp:
                if(isValid()){
                    signUpAPI();
                }
            break;

            case R.id.tvDOB:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener);
            break;
            case R.id.tvBottomLogin:
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            break;
        }
    }

    private boolean isValid()
    {
        if(etName.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter First Name.");
            return false;
        }else if(etLastName.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Last Name.");
            return false;
        }else if(tvDOB.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter DOB.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Mobile Number.");
            return false;
        }else if(etMobileNumber.getText().toString().trim().length()<10){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Mobile Number.");
            return false;
        }else if(etEmail.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Email/Username.");
            return false;
        }else if(!CommonUtils.isValidEmail(etEmail.getText().toString().trim())){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Email.");
            return false;
        }else if(selectedState.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Select State.");
            return false;
        }else if(selectedDistrict.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Select District.");
            return false;
        }else if(selectedWard.length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Select Ward.");
            return false;
        }else if(etPassword.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Password.");
            return false;
        }else if(!etConfirmPassword.getText().toString().trim().equalsIgnoreCase(etPassword.getText().toString().trim())){
            CommonUtils.setSnackbar(llMainView, "Password and Confirm Password should be same.");
            return false;
        }else{
            return true;
        }
    }

    private DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;

            Calendar selectedCal = Calendar.getInstance();
            selectedCal.set(mSelectedYear, mSelectedMonth, mSelectedDay);

            long selectedMilli = selectedCal.getTimeInMillis();
            Date datePickerDate = new Date(selectedMilli);
            if (datePickerDate.after(new Date())) {
                //Toast.makeText(NewOvertimeRequestActivity.this,"Cannot be in the future",Toast.LENGTH_LONG).show();
            }else{
                String month = ((mSelectedMonth + 1) > 9) ? "" + (mSelectedMonth + 1) : "0" + (mSelectedMonth + 1);
                String day = ((mSelectedDay) < 10) ? "0" + mSelectedDay : "" + mSelectedDay;

                //tvDOB.setText(day+"/"+month+"/"+mSelectedYear);
                tvDOB.setText(mSelectedYear+"-"+month+"-"+day);
                //tvFromDate.setText(mSelectedYear+"-"+month+"-"+day);

            }

        }
    };


    // initialize the DatePickerDialog
    private DatePickerDialog showDatePickerDialog(int initialYear, int initialMonth, int initialDay, DatePickerDialog.OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(SignupActivity.this, listener, initialYear, initialMonth, initialDay);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
        return dialog;
    }

    private void signUpAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            final JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email",etEmail.getText().toString().trim());
            jsonObject.addProperty("password",etPassword.getText().toString().trim());
            jsonObject.addProperty("firstName",etName.getText().toString().trim());
            jsonObject.addProperty("lastName",etLastName.getText().toString().trim());
            jsonObject.addProperty("dob",tvDOB.getText().toString().trim());
            jsonObject.addProperty("mobile",etMobileNumber.getText().toString().trim());
            jsonObject.addProperty("state",selectedState);
            jsonObject.addProperty("district",selectedDistrict);
            jsonObject.addProperty("ward",selectedWard);
            jsonObject.addProperty("deviceToken", cciiSession.getDeviceToken());
            jsonObject.addProperty("lat", String.valueOf(latitude));
            jsonObject.addProperty("long", String.valueOf(longitude));
            jsonObject.addProperty("type", selectedWardType);

            String url = RequestURL.SIGN_UP;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("true")){
                                    // CommonUtils.showToast(context, jsonResponse.message);
                                    CommonUtils.setSnackbar(llMainView, jsonResponse.message);

                                    //cciiSession.setSession(jsonResponse.userId,"", true);

                                    //Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));
        }

    }


    private void getStateAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
            //CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();

            String url = RequestURL.GET_STATE;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                   // CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){
                                    //CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    arlState.clear();
                                    arlDistrict.clear();
                                    if(jsonResponse.states!=null && jsonResponse.states.size()>0){
                                        arlState.addAll(jsonResponse.states);

                                    }

                                    adapterState.notifyDataSetChanged();

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }

    }

    private void getDistrictAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
           // CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();

            String url = RequestURL.GET_DISTRICT+selectedStateID;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                   // CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){
                                    //CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    arlDistrict.clear();
                                    arlWard.clear();
                                    if(jsonResponse.districts!=null && jsonResponse.districts.size()>0){
                                        arlDistrict.addAll(jsonResponse.districts);

                                    }

                                    adapterDistrit.notifyDataSetChanged();

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }

    }

    private void getWardAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
           // CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();
            selectedWardType.replace(" ", "%20");
            String wardTypeRemoveSpace=selectedWardType.replace(" ", "%20");

            String url = RequestURL.GET_WARD+"state="+selectedStateID+"&&district="+selectedDistrictID+"&&type="+wardTypeRemoveSpace;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                  //  CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("200")){
                                    //CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    arlWard.clear();
                                    if(jsonResponse.wards!=null && jsonResponse.wards.size()>0){
                                        arlWard.addAll(jsonResponse.wards);

                                    }

                                    adapterWard.notifyDataSetChanged();
                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermission()){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!statusOfGPS){
                showGPSAlert();
            }else{
                setUpdatedLocation();
            }
        }else {
            requestPermission();
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(SignupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }



    private void requestPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(SignupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(SignupActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(SignupActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(llMainView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(llMainView,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    /** this is method is used for display gps open alert */
    private void showGPSAlert(){
        if(gpsAlert==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Gps is disable please enable it")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                            startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            gpsAlert = builder.create();
            gpsAlert.show();
        }else{
            try {
                if(!gpsAlert.isShowing()){
                    gpsAlert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpdatedLocation(){
        com.ccii.utils.LocationTracker locationTracker = new com.ccii.utils.LocationTracker(SignupActivity.this, new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("","latitude "+ latitude + "longitude"+ longitude);

                }


            }
        });
        locationTracker.onUpdateLocation();
    }


}
