package com.ccii.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ccii.R;
import com.ccii.database.UserTable;
import com.ccii.responsedata.UserData;
import com.ccii.servicehelper.JsonResponse;
import com.ccii.servicehelper.RequestURL;
import com.ccii.servicehelper.ServiceAsync;
import com.ccii.servicehelper.ServiceStatus;
import com.ccii.utils.CCIISession;
import com.ccii.utils.CommonUtils;
import com.ccii.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Created by Ravi Shah on 6/15/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private ImageView ivUpdate;
    private EditText etEmail, etPassword;
    private TextView tvLogin,tvSignUp, tvBottomSignUp, tvForgotPassword;
    private LinearLayout llMainView;
    private Context context;
    private String inputTypeParameter="email";
    private CCIISession cciiSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        cciiSession = new CCIISession(context);
        getIds();
        setListenerrs();
    }

    private void getIds() {
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvBottomSignUp = (TextView) findViewById(R.id.tvBottomSignUp);
        llMainView = (LinearLayout) findViewById(R.id.llMainView);
        ivUpdate = (ImageView) findViewById(R.id.ivUpdate);
        ivUpdate.setColorFilter(getResources().getColor(R.color.white));

        final float ROTATE_FROM = 0.0f;
        final float ROTATE_TO = 350f;///-10.0f * 10.0f;
        RotateAnimation anim = new RotateAnimation(ROTATE_FROM, ROTATE_TO, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);//RotateAnimation(0f, 350f, 15f, 15f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(2000);
        ivUpdate.setAnimation(anim);

    }

    private void setListenerrs() {
        tvLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        tvBottomSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        ivUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ivUpdate:
                openNotifyDilog();
                break;

            case R.id.tvLogin:
                if(isValid()){
                    loginAPI();

                }
            break;

            case R.id.tvSignUp:
                    Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                    startActivity(intent);
                    finish();
            break;

            case R.id.tvBottomSignUp:
                    tvSignUp.performClick();
            break;

            case R.id.tvForgotPassword:
                showForgotPasswordDialog(LoginActivity.this);
            break;
        }
    }

    private boolean isValid()
    {
        if(etEmail.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Email OR Mobile No.");
            return false;
        }/*else if(!CommonUtils.isValidEmail(etEmail.getText().toString().trim())){
            CommonUtils.setSnackbar(llMainView, "Please Enter Valid Email.");
            return false;
        }*/else if(etPassword.getText().toString().trim().length()==0){
            CommonUtils.setSnackbar(llMainView, "Please Enter Password.");
            return false;
        }else{
            return true;
        }
    }

    private void loginAPI(){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();
            if(etEmail.getText().toString().trim().contains("@")){
                inputTypeParameter="email";
            }else{
                inputTypeParameter="mobile";
            }
            jsonObject.addProperty(inputTypeParameter,etEmail.getText().toString().trim());
            jsonObject.addProperty("password",etPassword.getText().toString().trim());

            String url = RequestURL.LOGIN+"email="+etEmail.getText().toString().trim()+"&&password="+etPassword.getText().toString().trim();

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_POST, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("true")){
                                    CommonUtils.setSnackbar(llMainView, jsonResponse.message);

                                    if(jsonResponse.user!=null){
                                        cciiSession.setSession(jsonResponse.user.userId,"", true);

                                        try {
                                            UserTable userTable = new UserTable(LoginActivity.this);
                                            userTable.insertToTable(jsonResponse.user, etPassword.getText().toString().trim(), etEmail.getText().toString().trim());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }



                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
           // CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));

            UserData userData = null;
            try {
                UserTable userTable = new UserTable(LoginActivity.this);
                userData=  userTable.dbLoginUser(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(userData!=null){
                cciiSession.setSession(userData.userId,"", true);
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }else{
                CommonUtils.setSnackbar(llMainView, "Invalid Login Credentials");
            }

        }

    }


    private void forgotPasswordAPI(String mobileNumber){

        if (CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.showProgressDialog(context);
            JsonObject jsonObject = new JsonObject();


            String url = RequestURL.FORGOT_PASSWORD+mobileNumber;

            CommonUtils.printLog("", "________________Request url___________" + url);

            new ServiceAsync(context, jsonObject.toString(), url, Constants.METHOD_GET, new ServiceStatus() {
                @Override
                public void onSuccess(Object o) {
                    CommonUtils.disMissProgressDialog(context);

                    String result = (String) o;
                    try {
                        if (result != null) {
                            CommonUtils.printLog("", "________________Response___________ " + result);
                            JsonResponse jsonResponse = new Gson().fromJson(result, JsonResponse.class);
                            if(jsonResponse!=null){
                                if(jsonResponse.status.equalsIgnoreCase("true")){
                                    CommonUtils.setSnackbar(llMainView, jsonResponse.message);

                                }else {
                                    try {
                                        CommonUtils.setSnackbar(llMainView, jsonResponse.message);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            try {
                                CommonUtils.setSnackbar(llMainView, getString(R.string.unable_connect));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onFailed(Object o) {
                    CommonUtils.disMissProgressDialog(context);
                    String message = (String) o;
                    CommonUtils.setSnackbar(llMainView, message);
                }
            }).execute();
        } else {
            CommonUtils.setSnackbar(llMainView, getString(R.string.no_network_connection));
        }

    }
    private void showForgotPasswordDialog(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_forgot_password, null);
        mDialog.setContentView(dialoglayout);

        final EditText etMobileNumber = (EditText)mDialog.findViewById(R.id.etMobileNumber);
        TextView tvSubmit = (TextView)mDialog.findViewById(R.id.tvSubmit);
        TextView tvCancel = (TextView)mDialog.findViewById(R.id.tvCancel);


        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etMobileNumber.getText().toString().trim().length()> 0 && etMobileNumber.getText().toString().trim().length()!=10){

                    try {
                        CommonUtils.setSnackbar(llMainView, "Please Enter Valid Mobile Number.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else   if(etMobileNumber.getText().toString().trim().length()==0){

                    try {
                        CommonUtils.setSnackbar(llMainView, "Please Enter Registered Mobile Number.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    mDialog.dismiss();
                    forgotPasswordAPI(etMobileNumber.getText().toString().trim());
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void openNotifyDilog() {
        final Dialog alertDialog = new Dialog(LoginActivity.this);
        alertDialog.requestWindowFeature(1);
        alertDialog.setContentView(R.layout.custom_alert_dialog);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        alertDialog.show();
        TextView tvok = (TextView) alertDialog.findViewById(R.id.tv_login);
        TextView tvCancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);

        TextView tvDilogMessage = (TextView) alertDialog.findViewById(R.id.tv_dilog_message);
        tvCancel.setVisibility(View.VISIBLE);
        tvDilogMessage.setText("New app update is available. Please press OK to download and install.");
        tvok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                //if (etKey.getText().toString().trim().equals(CommonMethods.getStringFromPreference(mContext, "key"))) {
                alertDialog.dismiss();
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://bitbucket.org/harry_bit/cciiappclone/downloads/CCIIAppClone.apk")));
					/*}else{
                        alertDialog.dismiss();
						CommonMethods.showCustomAlert(mContext,getString(R.string.app_key_alert), 0);
					}	*/


            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }
}
